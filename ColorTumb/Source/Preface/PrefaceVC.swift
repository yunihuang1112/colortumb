//
//  PrefaceVC.swift
//  ColorTumb
//
//  Created by YUNI on 2018/4/4.
//  Copyright © 2018年 YUNI. All rights reserved.
//

import UIKit

class PrefaceVC: BaseVC {
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    
    let LABEL_FONT : CGFloat = 30.0
    let LABEL_HEIGHT : CGFloat = 60.0
    let LABEL_FIRST_Y : CGFloat = 179.0 //60*7 = 420, 768 - 420 = 348, 348/2 = 179
    
    var txtArr : [String] = []
    var labelArr : [UILabel] = []
    var level : String = ""
    var btnName : String = ""
    var index : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        playMusic(musicName: DataObject.MUSIC_NAME_PASCAL)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.btnNext.alpha = 0
        self.btnNext.layer.cornerRadius = 10.0
        self.btnConfirm.alpha = 0
        self.btnConfirm.layer.cornerRadius = 10.0
        
        initTxtDic()
        for count in 0..<txtArr.count {
            createLabel(count: count)
        }
        showLabelsAnimation()
    }
    
    func initTxtDic() -> Void {
        txtArr = UITextConstant.PREFACE_STR
    }
    
    func showLabelsAnimation() -> Void {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.labelArr[self.index].alpha = 1
        }, completion: { _ in
            self.index += 1
            if (self.index < 7) {
                self.showLabelsAnimation()
            } else {
                self.showBtnAnimation()
            }
        })
    }
    
    func showBtnAnimation() -> Void {
        UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.btnConfirm.alpha = 1
            if (self.dataObj.getClearLevelOne() || self.dataObj.getClearLevelTwo()
                || self.dataObj.getClearLevelThree() || self.dataObj.getClearLevelFour()) {
                self.btnNext.alpha = 1
            }
        }, completion: nil)
    }
    
    func createLabel(count: Int) -> Void {
        let y : CGFloat = LABEL_FIRST_Y + LABEL_HEIGHT * CGFloat(count)
        let label = UILabel(frame: CGRect(x: 0, y: y, width: self.view.frame.width, height: LABEL_HEIGHT))
        label.center.x = self.view.center.x
        label.textAlignment = .center
        label.text = txtArr[count]
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: LABEL_FONT)
        label.alpha = 0
        self.view.addSubview(label)
        labelArr.append(label)
    }

    @IBAction func confirmBtnPressed(_ sender: Any) {
        appDelegate.audioPlayer = audioPlayer
        dataObj.setCurrentVC(vcName: ViewCtrlConstant.VC_PREFACE_VC)
        toNextPage(sender: self, storyboardName: ViewCtrlConstant.VC_FILLIN_VC)
    }
    
    @IBAction func nextBtnPressed(_ sender: Any) {
        toNextPage(sender: self, storyboardName: ViewCtrlConstant.VC_LEVEL_VC)
    }
}
