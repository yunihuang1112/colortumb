//
//  BaseVC.swift
//  ColorTumb
//
//  Created by YUNI on 2018/3/22.
//  Copyright © 2018年 YUNI. All rights reserved.
//

import UIKit
import PopupDialog
import MediaPlayer

class BaseVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {
    
    let dataObj : DataObject = DataObject.sharedInstance()
    let tableListObj = TableListObject.sharedInstance()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    let rightViewIdentifier : String = "RightView"
    let rightCellIdentifier : String = "RightTableCell"
    let rightCellHeight : CGFloat = 100.0
    let rightTableViewBorderWidth : CGFloat = 1.0
    let rightTableViewCornerRadius : CGFloat = 20.0
    let btnCornerRadius : CGFloat = 40.0
    
    var tableList : [String : Bool] = [:]
    var rightTableList : [UIImage] = []
    var rightListSelected : UIImage?
    var rightSelectedIndex : Int = 0
    var rightListSelectedImgName : String = ""
    var list : [String : UIImage] = [:]
    
    var isBagBtnPressed : Bool = false
    var isLevelClear : Bool = false
    
    var popupTitleStr : String = "title";
    var popupContentStr : String = "content content content content content";
    var popupImage : UIImage = UIImage.init()
    
    var audioPlayer : AVAudioPlayer?
    
    // MARK:
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        list = tableListObj.getImgList()
        NotificationCenter.default.addObserver(self, selector: #selector(audioInterrupted(_:)), name: Notification.Name.AVAudioSessionInterruption, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    internal func initTableList(type: String) {
        tableListObj.initList()
        tableList = tableListObj.getList(type: type)
        
        setRightTableList()
    }
    
    func setRightTableList() -> Void {
        rightTableList = []
        for data in tableList {
//            if (true) {
            if (data.value) {
                rightTableList.append(list[data.key]!)
            }
        }
    }
    
    // MARK:
    /* ===== Right Table View Delegate ===== */
    
    fileprivate func setRightTableViewDelegate(vc : UIViewController, rightView : RightView) -> Void {
        rightView.tableView.delegate = vc as? UITableViewDelegate
        rightView.tableView.dataSource = vc as? UITableViewDataSource
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rightTableList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rightCellHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: RightTableCell = Bundle.main.loadNibNamed(rightCellIdentifier, owner: self, options: nil)?.first as! RightTableCell
        cell.cellImageView?.image = rightTableList[indexPath.row]
        cell.backgroundColor = UIColor.clear
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCell : RightTableCell = tableView.cellForRow(at: indexPath) as! RightTableCell
        selectedCell.contentView.backgroundColor = UIColor.lightGray
        
        rightListSelected = rightTableList[indexPath.row]
        rightSelectedIndex = indexPath.row
        
        for data in list {
            if (data.value.isEqual(rightListSelected)) {
                rightListSelectedImgName = data.key
                break
            }
        }
    }
    
    // MARK:
    /* ===== Protected functions ===== */
    
    internal func initViewBorder(view: UIView) -> Void {
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.borderWidth = rightTableViewBorderWidth;
        view.layer.cornerRadius = rightTableViewCornerRadius;
    }
    
    internal func initRightView(vc: UIViewController, rightView: RightView) -> Void {
        for view in rightView.subviews {
            view.removeFromSuperview()
        }
        let customView = Bundle.main.loadNibNamed(rightViewIdentifier, owner: self, options: nil)!.first as! RightView
        rightView.addSubview(customView)
        setRightTableViewDelegate(vc: vc, rightView: customView)
        reloadRightTableView(rightView: customView)
        rightView.isHidden = true
    }
    
    internal func reloadRightTableView(rightView: RightView) -> Void {
        if (rightTableList.count == 0) {
            rightView.setEmptyLabelHidden(isHidden: false)
        } else {
            rightView.setEmptyLabelHidden(isHidden: true)
        }
        rightView.reloadTableView()
    }
    
    internal func initHintBtn(appendView: UIView) -> Void {
        let btn : UIButton = UIButton.init(frame: CGRect(x:848, y:680, width:80, height: 80))
        btn.backgroundColor = UIColor.init(red: 255.0/255.0, green: 255.0/255.0, blue: 127.0/255.0, alpha: 0.5)
        btn.layer.cornerRadius = btnCornerRadius
        btn.layer.borderWidth = rightTableViewBorderWidth
        btn.setBackgroundImage(UIImage.init(named: DataObject.IMG_PATH_ICON_HINT), for: UIControlState.normal)
        btn.addTarget(self, action: #selector(hintButtonPressed), for: UIControlEvents.touchUpInside)
        btn.tag = -1
        appendView.addSubview(btn)
    }
    
    @objc internal func hintButtonPressed(sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: ViewCtrlConstant.VC_HINT_VC)
        self.present(vc!, animated: true, completion: nil)
    }
    
    internal func initQBtn(appendView: UIView) -> Void {
        let btn : UIButton = UIButton.init(frame: CGRect(x:8, y:690, width:70, height: 70))
        btn.backgroundColor = UIColor.clear
        btn.setBackgroundImage(UIImage.init(named: DataObject.IMG_PATH_ICON_Q), for: UIControlState.normal)
        btn.addTarget(self, action: #selector(qButtonPressed), for: UIControlEvents.touchUpInside)
        btn.tag = -1
        appendView.addSubview(btn)
    }
    
    @objc func qButtonPressed(sender: UIButton) {
        popupView()
    }
    
    internal func initPopupLabel(title: String, content: String, image: UIImage) -> Void {
        popupTitleStr = title
        popupContentStr = content
        popupImage = image
    }
    
    internal func bagBtnPressed(sender: UIButton, rightView: RightView) {
        isBagBtnPressed = !isBagBtnPressed
        if (isBagBtnPressed) {
            rightView.isHidden = false
        } else {
            rightView.isHidden = true
        }
    }
    
    // MARK:
    
    fileprivate func stopPlayer() {
        if (audioPlayer != nil) {
            audioPlayer!.stop()
        }
    }
    
    fileprivate func stopAudioPlayer(_ sender: UIViewController) {
        if (sender.isKind(of: PrefaceVC.self)) {
            appDelegate.audioPlayer = audioPlayer
        } else if (sender.isKind(of: FillinVC.self)) {
            if (appDelegate.audioPlayer != nil) {
                appDelegate.audioPlayer!.stop()
                appDelegate.audioPlayer = nil
            }
            stopPlayer()
            
        } else {
            stopPlayer()
        }
    }
    
    // MARK:
    
    internal func toNextPage(sender: UIViewController, storyboardName: String) -> Void {
        stopAudioPlayer(sender)
        let vc = storyboard?.instantiateViewController(withIdentifier: storyboardName)
        sender.navigationController?.pushViewController(vc!, animated: true)
    }
    
    internal func popupView() -> Void {
        let title = popupTitleStr
        let message = popupContentStr
        let image = popupImage
        
        if (title == "" || message == "" || image == UIImage.init()) {
            print("popup error : no str or image")
            return
        }
        
        let popup = PopupDialog(title: title, message: message, image: image)
        let cancelBtn = CancelButton(title: UITextConstant.POPUP_HINT_BTN_OK) {
        }
        
        PopupDialogContainerView.appearance().backgroundColor = UIColor.clear
        CancelButton.appearance().titleColor  = UIColor.white
        CancelButton.appearance().titleFont   = UIFont.boldSystemFont(ofSize: 16)
        CancelButton.appearance().buttonColor = UIColor(red:0.5, green:0.5, blue:0.5, alpha:0.5)

        let dialogAppearance = PopupDialogDefaultView.appearance()
        dialogAppearance.backgroundColor      = UIColor(red:0.5, green:0.5, blue:0.5, alpha:0.5)
        dialogAppearance.titleFont            = UIFont.boldSystemFont(ofSize: 24)
        dialogAppearance.titleColor           = UIColor.white
        dialogAppearance.messageFont          = UIFont.systemFont(ofSize: 20)
        dialogAppearance.messageColor         = UIColor.white
        
        popup.addButtons([cancelBtn])
        
        self.present(popup, animated: true, completion: nil)
    }
    
    internal func playMusic(musicName: String) {
        do {
            let url = Bundle.main.url(forResource: musicName, withExtension: "mp3")
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            self.audioPlayer = try AVAudioPlayer(contentsOf: url!)
            self.audioPlayer?.numberOfLoops = -1
            if (self.audioPlayer != nil) {
                if (self.audioPlayer!.prepareToPlay()) {
//                    print("basevc start to play")
                    self.audioPlayer!.play()
                }
            }
        }
        catch {
            print("Error getting the audio file from basevc")
        }
    }
    
    internal func playFurtherMusic(musicName: String) {
        DispatchQueue.main.async {
            do {
                var furtherPlayer = AVAudioPlayer()
                let url = Bundle.main.url(forResource: musicName, withExtension: "mp3")
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                furtherPlayer = try AVAudioPlayer(contentsOf: url!)
                if (furtherPlayer.prepareToPlay()) {
                    print("further start to play")
                    furtherPlayer.play()
                }
            }
            catch {
            }
        }
    }
    
    @objc func audioInterrupted(_ notification: NSNotification) {
        //nothing haddler
    }
    
    internal func popErrorAlert(vc: UIViewController) -> Void {
        playFurtherMusic(musicName: DataObject.MUSIC_NAME_ERROR)
        alert(vc: vc, title: UITextConstant.ALERT_ERROR_TITLE, message: UITextConstant.ALERT_ERROR_MSG)
    }
    
    internal func alert(vc: UIViewController, title: String, message: String) -> Void {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: UITextConstant.POPUP_HINT_BTN_OK, style: .cancel, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
}
