//
//  RightView.swift
//  ColorTumb
//
//  Created by YUNI on 2018/3/24.
//  Copyright © 2018年 YUNI. All rights reserved.
//

import UIKit

class RightView: UIView {

    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyLabel: UILabel!
    
    override func draw(_ rect: CGRect) {
    }
    
    public func getTableView() -> UITableView {
        return self.tableView
    }
    
    public func reloadTableView() -> Void {
        self.tableView.reloadData()
    }
    
    public func setEmptyLabelHidden(isHidden : Bool) -> Void {
        self.emptyLabel.isHidden = isHidden
    }
}
