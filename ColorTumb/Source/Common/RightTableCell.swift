//
//  RightTableCell.swift
//  ColorTumb
//
//  Created by YUNI on 2018/3/24.
//  Copyright © 2018年 YUNI. All rights reserved.
//

import UIKit

class RightTableCell: UITableViewCell {

    @IBOutlet weak var cellImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
