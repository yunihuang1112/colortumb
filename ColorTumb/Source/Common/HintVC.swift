//
//  HintVC.swift
//  ColorTumb
//
//  Created by YUNI on 2018/4/6.
//  Copyright © 2018年 YUNI. All rights reserved.
//

import UIKit

class HintVC: BaseVC {

    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var btnBack: UIButton!
    
    let IMG_DIC_HINT_L1 : [String] = [DataObject.IMG_HINT_L1]
    let IMG_DIC_HINT_L3 : [String] = [DataObject.IMG_HINT_L3]
    let IMG_DIC_HINT_L4 : [String] = [DataObject.IMG_HINT_L4]
    
    let IMG_DIC_ANS_L1 : [String] = [DataObject.IMG_ANS_L1_1, DataObject.IMG_ANS_L1_2]
    let IMG_DIC_ANS_L2 : [String] = [DataObject.IMG_ANS_L2_1, DataObject.IMG_ANS_L2_2]
    let IMG_DIC_ANS_L3 : [String] = [DataObject.IMG_ANS_L3_1, DataObject.IMG_ANS_L3_2]
    let IMG_DIC_ANS_L4 : [String] = [DataObject.IMG_ANS_L4_1, DataObject.IMG_ANS_L4_2]
    
    let SCROLLVIEW_WIDTH : CGFloat = 750.0//638.0
    let SCROLLVIEW_HEIGHT : CGFloat = 488.0//415.0
    
    public var isClear : Bool = false
    
    var imgDic : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if (isClear){
            self.btnBack.setTitle("了解原理了！", for: UIControlState.normal)
        } else {
            self.btnBack.setTitle("回到關卡", for: UIControlState.normal)
        }
        initImgDic()
        initScrollView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    fileprivate func initImgDic() -> Void {
        let type : String = dataObj.getCurrentVC()
        if (type == ViewCtrlConstant.VC_FIRST_LEVEL_VC) {
            imgDic = IMG_DIC_HINT_L1
            if (isClear) {
                imgDic = IMG_DIC_ANS_L1
            }
        } else if (type == ViewCtrlConstant.VC_SECOND_LEVEL_VC) {
            if (isClear) {
                imgDic = IMG_DIC_ANS_L2
            }
        } else if (type == ViewCtrlConstant.VC_THIRD_LEVEL_VC) {
            imgDic = IMG_DIC_HINT_L3
            if (isClear) {
                imgDic = IMG_DIC_ANS_L3
            }
        } else if (type == ViewCtrlConstant.VC_FORTH_LEVEL_VC) {
            imgDic = IMG_DIC_HINT_L4
            if (isClear) {
                imgDic = IMG_DIC_ANS_L4
            }
        } else {
            imgDic = []
        }
    }
    
    fileprivate func initScrollView() -> Void {
        mainScrollView.delegate = self
        mainScrollView.isPagingEnabled = true
        for index in 0..<imgDic.count {
            let imgView : UIImageView = UIImageView.init(image: UIImage(named: imgDic[index]))
            imgView.frame.origin.x = SCROLLVIEW_WIDTH * CGFloat(index)
            imgView.frame.origin.y = 0
            imgView.frame.size.width = SCROLLVIEW_WIDTH
            imgView.frame.size.height = SCROLLVIEW_HEIGHT
            imgView.contentMode = .scaleAspectFit
            self.mainScrollView.addSubview(imgView)
        }
        
        self.mainScrollView.contentSize = CGSize(width: (SCROLLVIEW_WIDTH * CGFloat(imgDic.count)), height: SCROLLVIEW_HEIGHT)
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        if (isClear) {
            if (appDelegate.audioPlayer != nil) {
                appDelegate.audioPlayer!.stop()
                appDelegate.audioPlayer = nil
            }
            let vc = storyboard?.instantiateViewController(withIdentifier: ViewCtrlConstant.VC_FILLIN_VC)
            self.navigationController?.pushViewController(vc!, animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    

}
