//
//  AnswerVC.swift
//  ColorTumb
//
//  Created by YUNI on 2018/4/22.
//  Copyright © 2018年 YUNI. All rights reserved.
//

import UIKit

class AnswerVC: BaseVC {
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var pageControl : UIPageControl!
    @IBOutlet weak var btnNext: UIButton!
    
    let IMG_DIC_ANS_L1 : [String] = [DataObject.IMG_ANS_L1_1, DataObject.IMG_ANS_L1_2]
    let IMG_DIC_ANS_L2 : [String] = [DataObject.IMG_ANS_L2_1, DataObject.IMG_ANS_L2_2]
    let IMG_DIC_ANS_L3 : [String] = [DataObject.IMG_ANS_L3_1, DataObject.IMG_ANS_L3_2]
    let IMG_DIC_ANS_L4 : [String] = [DataObject.IMG_ANS_L4_1, DataObject.IMG_ANS_L4_2]
    
    let SCROLLVIEW_WIDTH : CGFloat = 984.0
    let SCROLLVIEW_HEIGHT : CGFloat = 683.0
    
    var imgDic : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
        initImgDic()
        initScrollView()
        initPageControl()
        
        if (appDelegate.audioPlayer != nil) {
            appDelegate.audioPlayer?.play()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    fileprivate func initView() -> Void {
        self.btnNext.isHidden = true
    }
    
    fileprivate func initImgDic() -> Void {
        let type : String = dataObj.getCurrentVC()
        if (type == ViewCtrlConstant.VC_FIRST_LEVEL_VC) {
            imgDic = IMG_DIC_ANS_L1
        } else if (type == ViewCtrlConstant.VC_SECOND_LEVEL_VC) {
            imgDic = IMG_DIC_ANS_L2
        } else if (type == ViewCtrlConstant.VC_THIRD_LEVEL_VC) {
            imgDic = IMG_DIC_ANS_L3
        } else if (type == ViewCtrlConstant.VC_FORTH_LEVEL_VC) {
            imgDic = IMG_DIC_ANS_L4
        } else {
            imgDic = []
        }
    }
    
    fileprivate func initScrollView() -> Void {
        mainScrollView.layer.cornerRadius = 10.0;
        mainScrollView.delegate = self
        mainScrollView.isPagingEnabled = true
        for index in 0..<imgDic.count {
            let imgView : UIImageView = UIImageView.init(image: UIImage(named: imgDic[index]))
            imgView.frame.origin.x = SCROLLVIEW_WIDTH * CGFloat(index)
            imgView.frame.origin.y = 0
            imgView.frame.size.width = SCROLLVIEW_WIDTH
            imgView.frame.size.height = SCROLLVIEW_HEIGHT
            imgView.contentMode = .scaleAspectFit
            self.mainScrollView.addSubview(imgView)
        }
        
        self.mainScrollView.contentSize = CGSize(width: (SCROLLVIEW_WIDTH * CGFloat(imgDic.count)), height: SCROLLVIEW_HEIGHT)
    }
    
    fileprivate func initPageControl() {
        self.pageControl.numberOfPages = imgDic.count
        self.pageControl.currentPage = 0
        self.pageControl.tintColor = UIColor.white
        pageControl.addTarget(self, action: #selector(changePage), for: UIControlEvents.valueChanged)
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(mainScrollView.contentOffset.x / mainScrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
        if (pageControl.currentPage == imgDic.count-1) {
            self.btnNext.isHidden = false
        } else {
            initView()
        }
    }
    
    @objc fileprivate func changePage(_ sender: Any) -> () {
        let x = CGFloat(pageControl.currentPage) * mainScrollView.frame.size.width
        mainScrollView.setContentOffset(CGPoint(x: x, y: 0), animated: true)
    }
    
    @IBAction func nextbtnPressed(_ sender: Any) {
        if (dataObj.getCurrentVC() == ViewCtrlConstant.VC_FORTH_LEVEL_VC) {
            UIView.animate(withDuration: 2.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                self.view.alpha = 0
            }, completion: { _ in
                self.toNextPage(sender: self, storyboardName: ViewCtrlConstant.VC_TREASURE_VC)
            })
            
        } else {
            if (appDelegate.audioPlayer != nil) {
                appDelegate.audioPlayer!.stop()
                appDelegate.audioPlayer = nil
            }
            let vc = storyboard?.instantiateViewController(withIdentifier: ViewCtrlConstant.VC_FILLIN_VC)
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }

}
