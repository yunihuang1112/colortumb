//
//  MainVC.swift
//  ColorTumb
//
//  Created by YUNI on 2018/3/22.
//  Copyright © 2018年 YUNI. All rights reserved.
//

import UIKit
import AudioToolbox


class MainVC: BaseVC {

    @IBOutlet weak var backgroundImgView: UIImageView!
    @IBOutlet weak var hintLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        playMusic(musicName: DataObject.MUSIC_NAME_THEME)
        initView()
    }
    
    func initView() -> Void {
        self.hintLabel.alpha = 0
        self.hintLabel.text = UITextConstant.MAIN_HINT_LABEL_TEXT;
        
        UIView.animate(withDuration: 1.5, delay: 2.0, options: [UIViewAnimationOptions.autoreverse, UIViewAnimationOptions.repeat], animations: {
                self.hintLabel.alpha = 1
        }, completion: nil)
        
        addTapGesture(view: self.backgroundImgView)
    }
    
    func addTapGesture(view : UIView) -> Void {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)));
        tap.numberOfTapsRequired = 1;
        view.addGestureRecognizer(tap);
        view.isUserInteractionEnabled = true;
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        appDelegate.navigationCtrl = self.navigationController
        dataObj.setCurrentVC(vcName: ViewCtrlConstant.VC_PREFACE_VC)
        toNextPage(sender: self, storyboardName: ViewCtrlConstant.VC_PREFACE_VC)
//        toNextPage(sender: self, storyboardName: ViewCtrlConstant.VC_FINISH_VC)
    }
}
