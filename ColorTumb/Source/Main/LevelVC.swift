//
//  LevelVC.swift
//  ColorTumb
//
//  Created by YUNI on 2018/4/24.
//  Copyright © 2018年 YUNI. All rights reserved.
//

import UIKit

class LevelVC: BaseVC {

    @IBOutlet weak var btnL1: UIButton!
    @IBOutlet weak var btnL2: UIButton!
    @IBOutlet weak var btnL3: UIButton!
    @IBOutlet weak var btnL4: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
        initData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    fileprivate func initView() -> Void {
        initBtn(btn: self.btnL1)
        initBtn(btn: self.btnL2)
        initBtn(btn: self.btnL3)
        initBtn(btn: self.btnL4)
    }
    
    fileprivate func initBtn(btn: UIButton) -> Void {
        btn.isUserInteractionEnabled = false
        btn.layer.cornerRadius = 10
        btn.alpha = 0.3
    }
    
    fileprivate func initData() -> Void {
        if (dataObj.getClearLevelOne()) {
            changeBtn(btn:self.btnL1)
        }
        if (dataObj.getClearLevelTwo()) {
            changeBtn(btn:self.btnL2)
        }
        if (dataObj.getClearLevelThree()) {
            changeBtn(btn:self.btnL3)
        }
        if (dataObj.getClearLevelFour()) {
            changeBtn(btn:self.btnL4)
        }
    }
    
    fileprivate func changeBtn(btn: UIButton) -> Void {
        btn.isUserInteractionEnabled = true
        btn.alpha = 1.0
        btn.backgroundColor = UIColor(red: 127.0/225.0, green: 0.0, blue: 0.0, alpha: 0.8)
    }

    @IBAction func l1BtnPressed(_ sender: Any) {
        dataObj.setCurrentVC(vcName: ViewCtrlConstant.VC_PREFACE_VC)
        toNextPage(sender: self, storyboardName: ViewCtrlConstant.VC_FILLIN_VC)
    }
    
    @IBAction func l2BtnPressed(_ sender: Any) {
        dataObj.setCurrentVC(vcName: ViewCtrlConstant.VC_FIRST_LEVEL_VC)
        toNextPage(sender: self, storyboardName: ViewCtrlConstant.VC_FILLIN_VC)
    }
    
    @IBAction func l3BtnPressed(_ sender: Any) {
        dataObj.setCurrentVC(vcName: ViewCtrlConstant.VC_SECOND_LEVEL_VC)
        toNextPage(sender: self, storyboardName: ViewCtrlConstant.VC_FILLIN_VC)
    }
    
    @IBAction func l4BtnPressed(_ sender: Any) {
        dataObj.setCurrentVC(vcName: ViewCtrlConstant.VC_THIRD_LEVEL_VC)
        toNextPage(sender: self, storyboardName: ViewCtrlConstant.VC_FILLIN_VC)
    }
    
}
