//
//  TableListObject.swift
//  ColorTumb
//
//  Created by YUNI on 2018/3/24.
//  Copyright © 2018年 YUNI. All rights reserved.
//

import UIKit

class TableListObject: NSObject {
    
    var tableList : [String : Bool] = [:]
    var imgList : [String : UIImage] = [:]
    var dataObj : DataObject = DataObject.sharedInstance()
    
    private static var mInstance:TableListObject?
    static func sharedInstance() -> TableListObject {
        if mInstance == nil {
            mInstance = TableListObject()
        }
        return mInstance!
    }
    
    public func getList(type : String) -> [String : Bool] {
        if (type == DataObject.TYPE_CRYSTAL) {
            tableList = dataObj.getCrystalDic()
        } else if (type == DataObject.TYPE_COLOR_KEY) {
            tableList = dataObj.getColorKeyDic()
        }
        return tableList
    }
    
    public func getImgList() -> [String : UIImage] {
//        return dataObj.getImgDic()
        return imgList
    }
    
    func initList(){
        if (dataObj.getCrystalDic().isEmpty) {
            initCrystalDatas()
            dataObj.setCrystalDic(dic: tableList)
        }
        
        if (dataObj.getColorKeyDic().isEmpty) {
            initKeyDatas()
            dataObj.setColorKeyDic(dic: tableList)
        }
        
//        if (dataObj.getImgDic().isEmpty) {
            initImgList()
//            dataObj.setImgDic(dic: imgList)
//        }
    }
    
    func setKeyValues(key : String, value : Bool) -> Void {
        tableList.updateValue(value, forKey: key)
    }
    
    func initCrystalDatas() -> Void {
        tableList = [:]
        setKeyValues(key: DataObject.COLOR_YELLOW, value: false)
        setKeyValues(key: DataObject.COLOR_YELLOW_ORANGE, value: false)
        setKeyValues(key: DataObject.COLOR_ORANGE, value: false)
        setKeyValues(key: DataObject.COLOR_ORANGE_RED, value: false)
        setKeyValues(key: DataObject.COLOR_RED, value: false)
        setKeyValues(key: DataObject.COLOR_RED_PURPLE, value: false)
        setKeyValues(key: DataObject.COLOR_PURPLE, value: false)
        setKeyValues(key: DataObject.COLOR_PURPLE_BLUE, value: false)
        setKeyValues(key: DataObject.COLOR_BLUE, value: false)
        setKeyValues(key: DataObject.COLOR_BLUE_GREEN, value: false)
        setKeyValues(key: DataObject.COLOR_GREEN, value: false)
        setKeyValues(key: DataObject.COLOR_GREEN_YELLOW, value: false)
    }
    
    func initKeyDatas() -> Void {
        tableList = [:]
        setKeyValues(key: DataObject.KEY_COLOR_YELLOW, value: true)
        setKeyValues(key: DataObject.KEY_COLOR_YELLOW_ORANGE, value: true)
        setKeyValues(key: DataObject.KEY_COLOR_ORANGE, value: true)
        setKeyValues(key: DataObject.KEY_COLOR_ORANGE_RED, value: true)
        setKeyValues(key: DataObject.KEY_COLOR_RED, value: true)
        setKeyValues(key: DataObject.KEY_COLOR_RED_PURPLE, value: true)
        setKeyValues(key: DataObject.KEY_COLOR_PURPLE, value: true)
        setKeyValues(key: DataObject.KEY_COLOR_PURPLE_BLUE, value: true)
        setKeyValues(key: DataObject.KEY_COLOR_BLUE, value: true)
        setKeyValues(key: DataObject.KEY_COLOR_BLUE_GREEN, value: true)
        setKeyValues(key: DataObject.KEY_COLOR_GREEN, value: true)
        setKeyValues(key: DataObject.KEY_COLOR_GREEN_YELLOW, value: true)
    }
    
    func initImgList() -> Void {
        imgList = [:]
        
        imgList.updateValue(newImg(name: DataObject.IMG_PATH_COLOR_YELLOW), forKey: DataObject.COLOR_YELLOW)
        imgList.updateValue(newImg(name: DataObject.IMG_PATH_COLOR_YELLOW_ORANGE), forKey: DataObject.COLOR_YELLOW_ORANGE)
        imgList.updateValue(newImg(name: DataObject.IMG_PATH_COLOR_ORANGE), forKey: DataObject.COLOR_ORANGE)
        imgList.updateValue(newImg(name: DataObject.IMG_PATH_COLOR_ORANGE_RED), forKey: DataObject.COLOR_ORANGE_RED)
        imgList.updateValue(newImg(name: DataObject.IMG_PATH_COLOR_RED), forKey: DataObject.COLOR_RED)
        imgList.updateValue(newImg(name: DataObject.IMG_PATH_COLOR_RED_PURPLE), forKey: DataObject.COLOR_RED_PURPLE)
        imgList.updateValue(newImg(name: DataObject.IMG_PATH_COLOR_PURPLE), forKey: DataObject.COLOR_PURPLE)
        imgList.updateValue(newImg(name: DataObject.IMG_PATH_COLOR_PURPLE_BLUE), forKey: DataObject.COLOR_PURPLE_BLUE)
        imgList.updateValue(newImg(name: DataObject.IMG_PATH_COLOR_BLUE), forKey: DataObject.COLOR_BLUE)
        imgList.updateValue(newImg(name: DataObject.IMG_PATH_COLOR_BLUE_GREEN), forKey: DataObject.COLOR_BLUE_GREEN)
        imgList.updateValue(newImg(name: DataObject.IMG_PATH_COLOR_GREEN), forKey: DataObject.COLOR_GREEN)
        imgList.updateValue(newImg(name: DataObject.IMG_PATH_COLOR_GREEN_YELLOW), forKey: DataObject.COLOR_GREEN_YELLOW)
                
        imgList.updateValue(newImg(name: DataObject.IMG_PATH_KEY_YELLOW), forKey: DataObject.KEY_COLOR_YELLOW)
        imgList.updateValue(newImg(name: DataObject.IMG_PATH_KEY_YELLOW_ORANGE), forKey: DataObject.KEY_COLOR_YELLOW_ORANGE)
        imgList.updateValue(newImg(name: DataObject.IMG_PATH_KEY_ORANGE), forKey: DataObject.KEY_COLOR_ORANGE)
        imgList.updateValue(newImg(name: DataObject.IMG_PATH_KEY_ORANGE_RED), forKey: DataObject.KEY_COLOR_ORANGE_RED)
        imgList.updateValue(newImg(name: DataObject.IMG_PATH_KEY_RED), forKey: DataObject.KEY_COLOR_RED)
        imgList.updateValue(newImg(name: DataObject.IMG_PATH_KEY_RED_PURPLE), forKey: DataObject.KEY_COLOR_RED_PURPLE)
        imgList.updateValue(newImg(name: DataObject.IMG_PATH_KEY_PURPLE), forKey: DataObject.KEY_COLOR_PURPLE)
        imgList.updateValue(newImg(name: DataObject.IMG_PATH_KEY_PURPLE_BLUE), forKey: DataObject.KEY_COLOR_PURPLE_BLUE)
        imgList.updateValue(newImg(name: DataObject.IMG_PATH_KEY_BLUE), forKey: DataObject.KEY_COLOR_BLUE)
        imgList.updateValue(newImg(name: DataObject.IMG_PATH_KEY_BLUE_GREEN), forKey: DataObject.KEY_COLOR_BLUE_GREEN)
        imgList.updateValue(newImg(name: DataObject.IMG_PATH_KEY_GREEN), forKey: DataObject.KEY_COLOR_GREEN)
        imgList.updateValue(newImg(name: DataObject.IMG_PATH_KEY_GREEN_YELLOW), forKey: DataObject.KEY_COLOR_GREEN_YELLOW)
    }
    
    func newImg(name : String) -> UIImage {
        return UIImage(named: name)!
    }
}

















