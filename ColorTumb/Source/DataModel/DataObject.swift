//
//  DataObject.swift
//  ColorTumb
//
//  Created by YUNI on 2018/3/22.
//  Copyright © 2018年 YUNI. All rights reserved.
//

import UIKit
import MediaPlayer

class DataObject: NSObject {
    
    var userDefaults : UserDefaults! = UserDefaults.standard
    
    private static var mInstance : DataObject?
    static func sharedInstance() -> DataObject {
        if mInstance == nil {
            mInstance = DataObject()
        }
        return mInstance!
    }
    
    // MARK:
    
    static let VERSION_RL = "RL"
    static let VERSION_DL = "DL"
    static let VERSION_CL = "CL"
    
    static let VERSION = "version"
    static let VERSION_APP = VERSION_DL
    static let CURRENT_VC = "currentVC"
    
    static let LEVEL_1 = "L1"
    static let LEVEL_2 = "L2"
    static let LEVEL_3 = "L3"
    static let LEVEL_4 = "L4"
    
    static let TYPE_CRYSTAL = "crystalType"
    static let TYPE_COLOR_KEY = "keyType"
    
    static let IMG_DIC = "imgDic"
    static let MUSIC = "music"
    static let GHOST_TYPE = "ghostType"
    
    // MARK:
    
    static let IMG_PATH_ICON_BAG = "icon_bag"
    static let IMG_PATH_ICON_HINT = "icon_hint"
    static let IMG_PATH_ICON_Q = "icon_q"
    static let IMG_PATH_ICON_Q_POPUP = "icon_q_pop"
    
    // MARK:
    
    static let IMG_PATH_BG_L2 = "bg_L2"
    static let IMG_PATH_BG_L2_BLANK = "bg_blank"
    static let IMG_PATH_BG_L3_PRE = "bg_L3_pre"
    static let IMG_PATH_BG_L3_POST = "bg_L3_post"
    static let IMG_CROWN = "crown"
    
    // MARK:
    
    static let USER_CRYSTALS = "userCrystals"
    static let CRYSTAL_DIC = "crystalDic"
    
    static let COLOR_YELLOW = "yellow"
    static let COLOR_YELLOW_ORANGE = "yellowOrange"
    static let COLOR_ORANGE = "orange"
    static let COLOR_ORANGE_RED = "orangeRed"
    static let COLOR_RED = "red"
    static let COLOR_RED_PURPLE = "redPurple"
    static let COLOR_PURPLE = "purple"
    static let COLOR_PURPLE_BLUE = "purpleBlue"
    static let COLOR_BLUE = "blue"
    static let COLOR_BLUE_GREEN = "blueGreen"
    static let COLOR_GREEN = "green"
    static let COLOR_GREEN_YELLOW = "greenYellow"
    
    static let IMG_PATH_COLOR_YELLOW = "crystal0"
    static let IMG_PATH_COLOR_YELLOW_ORANGE = "crystal1"
    static let IMG_PATH_COLOR_ORANGE = "crystal2"
    static let IMG_PATH_COLOR_ORANGE_RED = "crystal3"
    static let IMG_PATH_COLOR_RED = "crystal4"
    static let IMG_PATH_COLOR_RED_PURPLE = "crystal5"
    static let IMG_PATH_COLOR_PURPLE = "crystal6"
    static let IMG_PATH_COLOR_PURPLE_BLUE = "crystal7"
    static let IMG_PATH_COLOR_BLUE = "crystal8"
    static let IMG_PATH_COLOR_BLUE_GREEN = "crystal9"
    static let IMG_PATH_COLOR_GREEN = "crystal10"
    static let IMG_PATH_COLOR_GREEN_YELLOW = "crystal11"
    
    static let IMG_PATH_COLOR_PRESSED = "btn_l1_pressed";
    
    // MARK:
    
    static let KEY_DIC = "keyDic"
    
    static let KEY_COLOR_YELLOW = "yellowKey"
    static let KEY_COLOR_YELLOW_ORANGE = "yellowOrangeKey"
    static let KEY_COLOR_ORANGE = "orangeKey"
    static let KEY_COLOR_ORANGE_RED = "orangeRedKey"
    static let KEY_COLOR_RED = "redKey"
    static let KEY_COLOR_RED_PURPLE = "redPurpleKey"
    static let KEY_COLOR_PURPLE = "purpleKey"
    static let KEY_COLOR_PURPLE_BLUE = "purpleBlueKey"
    static let KEY_COLOR_BLUE = "blueKey"
    static let KEY_COLOR_BLUE_GREEN = "blueGreenKey"
    static let KEY_COLOR_GREEN = "greenKey"
    static let KEY_COLOR_GREEN_YELLOW = "greenYellowKey"
    
    static let IMG_PATH_KEY_YELLOW = "key1"
    static let IMG_PATH_KEY_YELLOW_ORANGE = "key2"
    static let IMG_PATH_KEY_ORANGE = "key3"
    static let IMG_PATH_KEY_ORANGE_RED = "key4"
    static let IMG_PATH_KEY_RED = "key5"
    static let IMG_PATH_KEY_RED_PURPLE = "key6"
    static let IMG_PATH_KEY_PURPLE = "key7"
    static let IMG_PATH_KEY_PURPLE_BLUE = "key8"
    static let IMG_PATH_KEY_BLUE = "key9"
    static let IMG_PATH_KEY_BLUE_GREEN = "key10"
    static let IMG_PATH_KEY_GREEN = "key11"
    static let IMG_PATH_KEY_GREEN_YELLOW = "key12"
    
    static let IMG_PATH_LOCK_YELLOW = "lock1"
    static let IMG_PATH_LOCK_YELLOW_ORANGE = "lock2"
    static let IMG_PATH_LOCK_ORANGE = "lock3"
    static let IMG_PATH_LOCK_ORANGE_RED = "lock4"
    static let IMG_PATH_LOCK_RED = "lock5"
    static let IMG_PATH_LOCK_RED_PURPLE = "lock6"
    static let IMG_PATH_LOCK_PURPLE = "lock7"
    static let IMG_PATH_LOCK_PURPLE_BLUE = "lock8"
    static let IMG_PATH_LOCK_BLUE = "lock9"
    static let IMG_PATH_LOCK_BLUE_GREEN = "lock10"
    static let IMG_PATH_LOCK_GREEN = "lock11"
    static let IMG_PATH_LOCK_GREEN_YELLOW = "lock12"
    
    // MARK:
    
    static let IMG_GHOST_R = "ghost_r"
    static let IMG_GHOST_G = "ghost_g"
    static let IMG_GHOST_B = "ghost_b"
    static let IMG_GHOST_C = "ghost_c"
    static let IMG_GHOST_M = "ghost_m"
    static let IMG_GHOST_Y = "ghost_y"
    static let IMG_GHOST_ANGRY = "ghost_angry"
    
    static let IMG_GHOST_GAIN_Y = "ghost_gain_y"
    static let IMG_GHOST_GAIN_R = "ghost_gain_r"
    static let IMG_GHOST_GAIN_G = "ghost_gain_g"
    static let IMG_GHOST_GAIN_B = "ghost_gain_b"
    
    // MARK:
    
    static let IMG_HINT_L1 = "hint_L1"
    static let IMG_HINT_L3 = "hint_L3"
    static let IMG_HINT_L4 = "hint_L4"
    
    static let IMG_ANS_L1_1 = "ans_L1_1"
    static let IMG_ANS_L1_2 = "ans_L1_2"
    static let IMG_ANS_L2_1 = "ans_L2_1"
    static let IMG_ANS_L2_2 = "ans_L2_2"
    static let IMG_ANS_L3_1 = "ans_L3_1"
    static let IMG_ANS_L3_2 = "ans_L3_2"
    static let IMG_ANS_L4_1 = "ans_L4_1"
    static let IMG_ANS_L4_2 = "ans_L4_2"
    
    // MARK:
    
    static let MUSIC_NAME_THEME = "Theme"
    static let MUSIC_NAME_PASCAL = "Pascal"
    static let MUSIC_NAME_BATTLE_1 = "Battle01"
    static let MUSIC_NAME_BATTLE_2 = "Battle02"
    static let MUSIC_NAME_HOPE = "Hope"
    static let MUSIC_NAME_ERROR = "error"
    
    // MARK:
    
    func setVersion(ver : String) -> Void {
        userDefaults.set(ver, forKey: DataObject.VERSION)
        userDefaults.synchronize()
    }
    
    func getVersion() -> String {
        return userDefaults.object(forKey: DataObject.VERSION) as! String
    }
    
    func setCurrentVC(vcName : String) -> Void {
        userDefaults.set(vcName, forKey: DataObject.CURRENT_VC)
        userDefaults.synchronize()
    }
    
    func getCurrentVC() -> String {
        return userDefaults.object(forKey: DataObject.CURRENT_VC) as! String
    }
    
    func setGhostType(type : String, value: Bool) -> Void {
        userDefaults.set(value, forKey: type)
        userDefaults.synchronize()
    }
    
    func getGhostType(type: String) -> Bool {
        return userDefaults.object(forKey: type) as! Bool
    }
    
    func setCrystalDic(dic : [String : Bool]) -> Void {
        userDefaults.set(dic, forKey: DataObject.CRYSTAL_DIC)
        userDefaults.synchronize()
    }
    
    func getCrystalDic() -> [String : Bool] {
        if (userDefaults.object(forKey: DataObject.CRYSTAL_DIC) == nil) {
            return [:]
        }
        return userDefaults.object(forKey: DataObject.CRYSTAL_DIC) as! [String : Bool]
    }
    
    func setColorKeyDic(dic : [String : Bool]) -> Void {
        userDefaults.set(dic, forKey: DataObject.KEY_DIC)
        userDefaults.synchronize()
    }
    
    func getColorKeyDic() -> [String : Bool] {
        if (userDefaults.object(forKey: DataObject.KEY_DIC) == nil) {
            return [:]
        }
        return userDefaults.object(forKey: DataObject.KEY_DIC) as! [String : Bool]
    }
    
    func setImgDic(dic : [String : UIImage]) -> Void {
        userDefaults.set(dic, forKey: DataObject.IMG_DIC)
        userDefaults.synchronize()
    }
    
    func getImgDic() -> [String : UIImage] {
        if (userDefaults.object(forKey: DataObject.IMG_DIC) == nil) {
            return [:]
        }
        return userDefaults.object(forKey: DataObject.IMG_DIC) as! [String : UIImage]
    }
    
    // MARK:
    
    func setUserCrystals(key: Int, value: String) -> Void {
        userDefaults.set(value, forKey: DataObject.USER_CRYSTALS + String(format: "%i", key))
        userDefaults.synchronize()
    }
    
    func getUserCrystals(key: Int) -> String {
        if (userDefaults.object(forKey: DataObject.USER_CRYSTALS + String(format: "%i", key)) == nil) {
            return ""
        }
        return userDefaults.object(forKey: DataObject.USER_CRYSTALS + String(format: "%i", key)) as! String
    }
    
    func setClearLevelOne(isClear : Bool) -> Void {
        userDefaults.set(isClear, forKey: DataObject.LEVEL_1)
        userDefaults.synchronize()
    }
    
    func getClearLevelOne() -> Bool {
        if (userDefaults.object(forKey: DataObject.LEVEL_1) == nil) {
            return false
        }
        return userDefaults.object(forKey: DataObject.LEVEL_1) as! Bool
    }
    
    func setClearLevelTwo(isClear : Bool) -> Void {
        userDefaults.set(isClear, forKey: DataObject.LEVEL_2)
        userDefaults.synchronize()
    }
    
    func getClearLevelTwo() -> Bool {
        if (userDefaults.object(forKey: DataObject.LEVEL_2) == nil) {
            return false
        }
        return userDefaults.object(forKey: DataObject.LEVEL_2) as! Bool
    }
    
    func setClearLevelThree(isClear : Bool) -> Void {
        userDefaults.set(isClear, forKey: DataObject.LEVEL_3)
        userDefaults.synchronize()
    }
    
    func getClearLevelThree() -> Bool {
        if (userDefaults.object(forKey: DataObject.LEVEL_3) == nil) {
            return false
        }
        return userDefaults.object(forKey: DataObject.LEVEL_3) as! Bool
    }
    
    func setClearLevelFour(isClear : Bool) -> Void {
        userDefaults.set(isClear, forKey: DataObject.LEVEL_4)
        userDefaults.synchronize()
    }
    
    func getClearLevelFour() -> Bool {
        if (userDefaults.object(forKey: DataObject.LEVEL_4) == nil) {
            return false
        }
        return userDefaults.object(forKey: DataObject.LEVEL_4) as! Bool
    }
}












