//
//  ViewCtrlObject.swift
//  ColorTumb
//
//  Created by YUNI on 2018/3/22.
//  Copyright © 2018年 YUNI. All rights reserved.
//

import UIKit

class ViewCtrlConstant: NSObject {
    static let VC_MAIN_VC = "MainVC";
    static let VC_PREFACE_VC = "PrefaceVC";
    static let VC_FILLIN_VC = "FillinVC";
    static let VC_FIRST_LEVEL_VC = "FirstLevelVC";
    static let VC_COLOR_DETECTION_VC = "ColorDetectionVC";
    static let VC_SECOND_LEVEL_VC = "SecondLevelVC";
    static let VC_THIRD_LEVEL_VC = "ThirdLevelVC";
    static let VC_FORTH_LEVEL_VC = "ForthLevelVC";
    static let VC_TREASURE_VC = "TreasureVC";
    static let VC_SELFISH_VC = "SelfishVC";
    static let VC_GHOST_DIALOG_VC = "GhostDialogVC";
    static let VC_FINISH_VC = "FinishVC";
    static let VC_HINT_VC = "HintVC";
    static let VC_ANSWER_VC = "AnswerVC";
    static let VC_LEVEL_VC = "LevelVC";
}
