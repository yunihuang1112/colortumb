//
//  UITextConstant.swift
//  ColorTumb
//
//  Created by YUNI on 2018/3/22.
//  Copyright © 2018年 YUNI. All rights reserved.
//

import UIKit

class UITextConstant: NSObject {
    static let MAIN_HINT_LABEL_TEXT = "任意點擊畫面開始遊戲";
    
    static let INPUT_TEXT_WARM = "很暖";
    static let INPUT_TEXT_COLD = "很冷";
    static let INPUT_TEXT_NONE = "很中立";
    static let GHOST_DIALOG_NONE_MSG_1 = "你，不是，已經...";
    static let GHOST_DIALOG_NONE_MSG_2 = "跟我說過話了嗎！！！！！";
    static let GHOST_DIALOG_WARM = "我是鬼魂界的「暖心派」，所以你們這些人類看到我應該都覺得...";
    static let GHOST_DIALOG_COLD = "我是鬼魂界的「冷心派」，所以你們這些人類看到我應該都覺得...";
    static let GHOST_DIALOG_NONE = "我是鬼魂界的「中立派」，所以你們這些人類看到我應該都覺得...";
    static let GHOST_DIALOG_FEEL_WARM = "非！常！非！常！熱！熱！！熱！！！";
    static let GHOST_DIALOG_FEEL_COLD = "非！常！非！常！冷！冷！！冷！！！";
    static let GHOST_DIALOG_FEEL_NONE = "普通到不行。";
    
    static let ALERT_ERROR_TITLE = "錯誤"
    static let ALERT_ERROR_MSG = "解鎖錯誤"
    static let ALERT_BTN_OK = "確定";
    
    static let POPUP_GHOST_TITLE = "關卡AR：恭喜";
    static let POPUP_GHOST_CONTENT = "恭喜你獲得水晶球。";
    
    static let POPUP_HINT_BTN_OK = "確定";
    
    static let POPUP_LEVEL1_TITLE = "關卡一 : 環狀鎖";
    static let POPUP_LEVEL1_CONTENT = "請將所獲得之水晶球依特定順序放入環狀中。\n（水晶球可以從遺跡外獲得）";
    
    static let POPUP_LEVEL2_TITLE = "關卡二 : 渲染";
    static let POPUP_LEVEL2_CONTENT = "請試著渲染池水之顏色，並獲得鑰匙。";
    
    static let POPUP_LEVEL3_TITLE = "關卡三 : 解鎖";
    static let POPUP_LEVEL3_CONTENT = "請利用所獲得之鑰匙解開所有的鎖。";
    
    static let POPUP_LEVEL4_TITLE = "關卡四 : 匯集";
    static let POPUP_LEVEL4_CONTENT = "滑動方塊內容並選擇正確的顏色，以成功匯集需要的顏色。\n（左方為燈光匯集，右方為顏料匯集）";
    
    static let POPUP_TREASURE_TITLE = "最終 : 寶藏";
    static let POPUP_TREASURE_CONTENT = "恭喜您獲得最終寶藏\n\n十二色皇冠";
    static let TREASURE_TXTVIEW_TEXT = "    在各位獵人的努力之下，歷經重重關卡，並順利地解決，您將獲得最終寶藏。";
    
    static let PREFACE_BTN = "動身出發";
    static let FILLIN_BTN = "前往關卡";
    
    static let PREFACE_STR : [String] = ["您是一位出身於顯赫貴族的寶藏獵人，",
                                         "在一次的宴會中，",
                                         "意外獲得一張稀有遺跡的藏寶圖，",
                                         "該遺跡名為「彩墓」，",
                                         "您必須與你的團隊馬上動身前往該遺跡並獲得其寶藏，",
                                         "最後帶著戰利品光榮回歸。",
                                         "如果已經準備齊全，請點選出發之按鈕。"]
    
    static let FILLIN_LEVEL_1 : [String] = ["根據所獲得的藏寶圖，",
                                            "經過長途跋涉，",
                                            "終於來到遺跡，",
                                            "遺跡外看起來很一般遺跡沒什麼不一樣，",
                                            "而這裡是遺跡入口，",
                                            "上面有個神奇的大鎖，",
                                            "看起來需要先解開它才能進入遺跡。"]
    
    static let FILLIN_LEVEL_2 : [String] = ["終於打開大門進入遺跡，",
                                            "印入眼簾的是一些骷顱頭以及石頭，",
                                            "跟大部分的遺跡一樣，",
                                            "不過特別的是，",
                                            "前方有一個小小的池塘，",
                                            "看起來有甚麼玄機，",
                                            "來觀察觀察。"]
    
    static let FILLIN_LEVEL_3 : [String] = ["池水的變化引起了許多的泡泡，",
                                            "並連帶給予了不同的鑰匙，",
                                            "這些鑰匙上面嵌著剛剛大門上的水晶，",
                                            "看來是個線索。",
                                            "走著走著，",
                                            "發現前方路被擋住了，",
                                            "看來需要想想看怎麼過去了。"]
    
    static let FILLIN_LEVEL_4 : [String] = ["果然鑰匙跟鎖是相對的，",
                                            "經過一番努力終於解開了所有的鎖。",
                                            "通過那個小房間後，來到了一個較為黑暗的區域，",
                                            "但卻有兩個機關，",
                                            "左邊有燈光，且三道燈光會匯集在一處，",
                                            "右邊則是顏料，三種顏料也有混在一起的部分，",
                                            "看來需要思考一番了。"]
    
    static let FINISH : [String] = ["經過漫長且艱辛的一趟，",
                                    "古墓之旅，",
                                    "終於帶著寶藏回歸。",
                                    "",
                                    "過了一段時間，",
                                    "我們帶著寶藏出席宴會，",
                                    "所有人為之震驚，",
                                    "拍手叫好，",
                                    "再次證明了我們寶藏獵人一族的實力。",
                                    "",
                                    "",
                                    "非常感謝你們的幫忙，",
                                    "",
                                    "沒有你們，",
                                    "",
                                    "就沒有現在。",
                                    "",
                                    "",
                                    "帶著你們的這份熱情，",
                                    "",
                                    "前往下一個目的地，",
                                    "",
                                    "你們會獲得該有的好成績的。",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "Developer、Designer",
                                    "Yuni Huang",
                                    "",
                                    "",
                                    "Resources",
                                    "Graphics",
                                    "http://raidingtheglobe.com/gallery/rise-of-the-tomb-raider/concept-art",
                                    "",
                                    "Music",
                                    "https://www.youtube.com/watch?v=CbZjwid9FHs",
                                    "https://www.youtube.com/watch?v=M06yl1wupn8",
                                    "https://www.youtube.com/watch?v=xcSSsa6AE2M",
                                    "https://www.youtube.com/watch?v=w6nTig6s38Q",
                                    "https://www.youtube.com/watch?v=rLqY3b1vOFE",
                                    "https://www.youtube.com/watch?v=vBMdAbk5cMg",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "Thanks",
                                    "",
                                    "國立台灣師範大學",
                                    "王健華 教授",
                                    "",
                                    "Annette Lee",
                                    "Bonnie Chao",
                                    "",
                                    "台北市立建成國中",
                                    "陳海新 老師",
                                    "楊惠玲 老師",
                                    "",
                                    "&",
                                    "",
                                    "YOU"
    ]
}




















