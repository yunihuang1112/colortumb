//
//  FillinVC.swift
//  ColorTumb
//
//  Created by YUNI on 2018/4/9.
//  Copyright © 2018年 YUNI. All rights reserved.
//

import UIKit

class FillinVC: BaseVC {
    @IBOutlet weak var btnConfirm: UIButton!
    
    let LABEL_FONT : CGFloat = 30.0
    let LABEL_HEIGHT : CGFloat = 60.0
    let LABEL_FIRST_Y : CGFloat = 179.0 //60*7 = 420, 768 - 420 = 348, 348/2 = 179
    
    var txtArr : [String] = []
    var labelArr : [UILabel] = []
    var level : String = ""
    var btnName : String = ""
    var index : Int = 0
    var nextVC : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.viewControllers = [self]
        
        if (appDelegate.audioPlayer == nil) {
            playMusic(musicName: DataObject.MUSIC_NAME_PASCAL)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.btnConfirm.alpha = 0
        self.btnConfirm.layer.cornerRadius = 10.0
        
        initTxtDic()
        if (txtArr.count != 0) {
            for count in 0..<txtArr.count {
                createLabel(count: count)
            }
            showLabelsAnimation()
        }
    }

    func initTxtDic() -> Void {
        let type : String = dataObj.getCurrentVC()
        if (type == ViewCtrlConstant.VC_PREFACE_VC) {
            txtArr = UITextConstant.FILLIN_LEVEL_1
            nextVC = ViewCtrlConstant.VC_FIRST_LEVEL_VC
        } else if (type == ViewCtrlConstant.VC_FIRST_LEVEL_VC) {
            txtArr = UITextConstant.FILLIN_LEVEL_2
            nextVC = ViewCtrlConstant.VC_SECOND_LEVEL_VC
        } else if (type == ViewCtrlConstant.VC_SECOND_LEVEL_VC) {
            txtArr = UITextConstant.FILLIN_LEVEL_3
            nextVC = ViewCtrlConstant.VC_THIRD_LEVEL_VC
        } else if (type == ViewCtrlConstant.VC_THIRD_LEVEL_VC) {
            txtArr = UITextConstant.FILLIN_LEVEL_4
            nextVC = ViewCtrlConstant.VC_FORTH_LEVEL_VC
        } else {
            txtArr = []
        }
    }
    
    func showLabelsAnimation() -> Void {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.labelArr[self.index].alpha = 1
        }, completion: { _ in
            self.index += 1
            if (self.index < 7) {
                self.showLabelsAnimation()
            } else {
                self.showBtnAnimation()
            }
        })
    }
    
    func showBtnAnimation() -> Void {
        UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.btnConfirm.alpha = 1
        }, completion: nil)
    }
    
    func createLabel(count: Int) -> Void {
        let y : CGFloat = LABEL_FIRST_Y + LABEL_HEIGHT * CGFloat(count)
        let label = UILabel(frame: CGRect(x: 0, y: y, width: self.view.frame.width, height: LABEL_HEIGHT))
        label.center.x = self.view.center.x
        label.textAlignment = .center
        label.text = txtArr[count]
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: LABEL_FONT)
        label.alpha = 0
        self.view.addSubview(label)
        labelArr.append(label)
    }
    
    @IBAction func confirmBtnPressed(_ sender: Any) {
        toNextPage(sender: self, storyboardName: nextVC)
    }
}
