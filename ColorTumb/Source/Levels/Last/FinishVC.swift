//
//  FinishVC.swift
//  ColorTumb
//
//  Created by YUNI on 2018/4/18.
//  Copyright © 2018年 YUNI. All rights reserved.
//

import UIKit

class FinishVC: BaseVC {
    @IBOutlet weak var scrollView: UIScrollView!
    
    let LABEL_FONT : CGFloat = 30.0
    let LABEL_HEIGHT : CGFloat = 60.0
    let LABEL_FIRST_Y : CGFloat = 179.0 //60*7 = 420, 768 - 420 = 348, 348/2 = 179
    
    var txtArr : [String] = []
    var currentY : CGFloat = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let vc = storyboard?.instantiateViewController(withIdentifier: ViewCtrlConstant.VC_MAIN_VC)
        self.navigationController?.viewControllers = [vc!, self]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        playMusic(musicName: DataObject.MUSIC_NAME_HOPE)
        txtArr = UITextConstant.FINISH
        createView()
        createView()
        if (txtArr.count != 0) {
            for count in 0..<txtArr.count {
                createLabel(count: count)
            }
            currentY = currentY + LABEL_HEIGHT * CGFloat(txtArr.count) + LABEL_FIRST_Y
        }
        createView()
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height: currentY + self.scrollView.frame.height)
        showScrollViewAnimation()
    }
    
    func createView() -> Void {
        let view : UIView = UIView.init(frame: self.scrollView.frame)
        view.frame.origin.y = currentY
        if (currentY == 0) {
            currentY = self.scrollView.frame.origin.y + self.scrollView.frame.size.height
        }
        view.alpha = 0
        self.scrollView.addSubview(view)
    }

    func createLabel(count: Int) -> Void {
        let y : CGFloat = currentY + LABEL_FIRST_Y + LABEL_HEIGHT * CGFloat(count)
        let label = UILabel(frame: CGRect(x: 0, y: y, width: self.view.frame.width, height: LABEL_HEIGHT))
        label.center.x = self.view.center.x
        label.textAlignment = .center
        label.text = txtArr[count]
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: LABEL_FONT)
        self.scrollView.addSubview(label)
    }
    
    func showScrollViewAnimation() -> Void {
        DispatchQueue.main.async() {
            UIView.animate(withDuration: 66.0, delay: 0.5, options: UIViewAnimationOptions.curveLinear, animations: {
                self.scrollView.contentOffset.y = self.scrollView.contentSize.height
            }, completion: { result in
                self.navigationController?.popToRootViewController(animated: true)
            })
        }
        
    }
}
