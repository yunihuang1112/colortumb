//
//  SelfishVC.swift
//  ColorTumb
//
//  Created by YUNI on 2018/4/8.
//  Copyright © 2018年 YUNI. All rights reserved.
//

import UIKit
import CoreImage

class SelfishVC: BaseVC, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    @IBOutlet weak var hintLabel: UILabel!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet weak var overlayImageView: UIImageView!
    @IBOutlet weak var btnCapture: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnDeleteImg: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func captureBtnPressed(_ sender: Any) {
        callGetPhoneWithKind(1)
    }
    
    @IBAction func saveBtnPressed(_ sender: Any) {
        if (self.imageView.image == nil) {
            alert(vc: self, title: "錯誤", message: "尚未拍照")
        } else {
            let alert = UIAlertController(title: "確定是這張嗎？", message: nil, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "取消", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: UITextConstant.POPUP_HINT_BTN_OK, style: .default, handler: { action in
                UIImageWriteToSavedPhotosAlbum(self.takeSnapshot(), self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    @IBAction func deleteImgBtnPressed(_ sender: Any) {
        for subView : UIView in self.imageView.subviews {
            subView.removeFromSuperview()
        }
        self.imageView.image = nil
        self.overlayImageView.isHidden = true
    }
    
    func takeSnapshot() -> UIImage {
        let tempImgView : UIImageView = UIImageView.init(frame: self.overlayImageView.frame)
//        tempImgView.frame.origin.y = 0
//        tempImgView.frame.size.height -= 20.0
        let overlayTempImgView : UIImageView = self.overlayImageView
        overlayTempImgView.frame.origin.x = 0
        let picTempImgView : UIImageView = self.imageView
        picTempImgView.center = overlayTempImgView.center
        tempImgView.addSubview(picTempImgView)
        tempImgView.addSubview(overlayTempImgView)
        
        UIGraphicsBeginImageContextWithOptions(tempImgView.bounds.size, false, UIScreen.main.scale);
        tempImgView.drawHierarchy(in: tempImgView.bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return image!;
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            alert(vc: self, title: "儲存失敗！", message: "請重新儲存。")
            print(error)
        } else {
            let alert = UIAlertController(title: "儲存成功！", message: "已儲存至本機。", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: UITextConstant.POPUP_HINT_BTN_OK, style: .cancel, handler: { action in
                self.toNextPage(sender: self, storyboardName: ViewCtrlConstant.VC_FINISH_VC)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func callGetPhoneWithKind(_ kind: Int) {
        let picker: UIImagePickerController = UIImagePickerController()
        switch kind {
        case 1:
            // 開啟相機
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                picker.sourceType = UIImagePickerControllerSourceType.camera
                picker.allowsEditing = true
                picker.delegate = self
                self.present(picker, animated: true, completion: nil)
            } else {
                print("沒有相機鏡頭...")
            }
        default:
            // 開啟相簿
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                picker.allowsEditing = true
                picker.delegate = self
                self.present(picker, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: Delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil) // 關掉
        for subView : UIView in self.imageView.subviews {
            subView.removeFromSuperview()
        }
        self.imageView.image = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
        self.overlayImageView.isHidden = false
//        detect()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    // MARK: detect
    func detect() {
//        let crownImgView : UIImageView = UIImageView.init(image: UIImage(named: DataObject.IMG_CROWN))
//        crownImgView.frame = self.imageView.frame
//        crownImgView.frame.origin.y = self.imageView.frame.origin.y + self.imageView.frame.size.height / CGFloat(2)
//        crownImgView.frame.size.height = self.imageView.frame.size.height / CGFloat(2)
//        crownImgView.contentMode = .scaleAspectFit
//        self.imageView.addSubview(crownImgView)
//        guard let personciImage = CIImage(image: self.imageView.image!) else {
//            return
//        }
//
//        let accuracy = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
//        let faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: accuracy)
//        let faces = faceDetector?.features(in: personciImage)
//
//        // 用來將 Core Image 座標轉換成 UIView 座標
//        let ciImageSize = personciImage.extent.size
//        var transform = CGAffineTransform(scaleX: 1, y: -1)
//        transform = transform.translatedBy(x: 0, y: -ciImageSize.height)
//
//        if (faces?.count == 0) {
//            self.imageView.image = nil
//            alert(vc: self, title: "沒有臉啊啊啊", message: "請確實拍到臉。")
//            return
//        }
//
//        for face in faces as! [CIFaceFeature] {
//            print("Found bounds are \(face.bounds)")
//
//            // 套用座標轉換實作
//            var faceViewBounds = face.bounds.applying(transform)
//
//            // 計算矩形在 imageView 中的實際位置和大小
//            let viewSize = self.imageView.bounds.size
//            let scale = min(viewSize.width / ciImageSize.width,
//                            viewSize.height / ciImageSize.height)
//            let offsetX = (viewSize.width - ciImageSize.width * scale) / 2
//            let offsetY = (viewSize.height - ciImageSize.height * scale) / 2
//
//            faceViewBounds = faceViewBounds.applying(CGAffineTransform(scaleX: scale, y: scale))
//            faceViewBounds.origin.x += offsetX
//            faceViewBounds.origin.y += offsetY
//
////            let faceBox = UIView(frame: faceViewBounds)
////            faceBox.layer.borderWidth = 3
////            faceBox.layer.borderColor = UIColor.red.cgColor
////            faceBox.backgroundColor = UIColor.clear
////            self.imageView.addSubview(faceBox)
//
//            let crownImgView : UIImageView = UIImageView.init(image: UIImage(named: DataObject.IMG_CROWN))
//            crownImgView.frame = self.imageView.frame
//            crownImgView.frame.origin.y = self.imageView.center.y
//            crownImgView.frame.size.height = self.imageView.frame.size.height / CGFloat(2)
////            crownImgView.frame.origin.x = faceViewBounds.origin.x + CGFloat(5.0)
////            crownImgView.frame.origin.y = faceViewBounds.origin.y - faceBox.frame.origin.y
//            crownImgView.contentMode = .scaleAspectFit
//            self.imageView.addSubview(crownImgView)
//
////            if face.hasLeftEyePosition {
////                print("Left eye bounds are \(face.leftEyePosition)")
////            }
////
////            if face.hasRightEyePosition {
////                print("Right eye bounds are \(face.rightEyePosition)")
////            }
//        }
    }
}


