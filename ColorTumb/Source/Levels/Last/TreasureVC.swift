//
//  TreasureVC.swift
//  ColorTumb
//
//  Created by YUNI on 2018/4/8.
//  Copyright © 2018年 YUNI. All rights reserved.
//

import UIKit
import PopupDialog

class TreasureVC: BaseVC {

    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var mainTextView: UITextView!
    @IBOutlet weak var mainBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        playMusic(musicName: DataObject.MUSIC_NAME_HOPE)
        initView()
        showImgAnimation()
        showTextAnimation()
        showBtnAnimation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func initView() {
        mainBtn.tintColor = UIColor.clear
        mainBtn.backgroundColor = UIColor.init(red: 50.0/255.0, green: 0.0, blue: 0.0, alpha: 0.0)
        mainBtn.isUserInteractionEnabled = true
        
        mainTextView.alpha = 0
        let str = UITextConstant.TREASURE_TXTVIEW_TEXT
        let paraph = NSMutableParagraphStyle()
        paraph.lineSpacing = 20
        let attributes = [NSAttributedStringKey.font:UIFont.systemFont(ofSize: 30),
                          NSAttributedStringKey.paragraphStyle: paraph]
        mainTextView.attributedText = NSAttributedString(string: str, attributes: attributes)
        mainTextView.textColor = UIColor.white
        mainTextView.backgroundColor = UIColor.clear
    }

    @IBAction func mainBtnPressed(_ sender: Any) {
        popup()
    }
    
    fileprivate func popup() -> Void {
        let title = UITextConstant.POPUP_TREASURE_TITLE
        let message = UITextConstant.POPUP_TREASURE_CONTENT
        let image = UIImage(named: DataObject.IMG_CROWN)!
        
        let popup = PopupDialog(title: title, message: message, image: image)
        let cancelBtn = CancelButton(title: UITextConstant.POPUP_HINT_BTN_OK) {
//            self.toNextPage(sender: self, storyboardName: ViewCtrlConstant.VC_SELFISH_VC)
            self.toNextPage(sender: self, storyboardName: ViewCtrlConstant.VC_FINISH_VC)
        }
        
        PopupDialogContainerView.appearance().backgroundColor = UIColor.clear
        CancelButton.appearance().titleColor  = UIColor.white
        CancelButton.appearance().titleFont   = UIFont.boldSystemFont(ofSize: 16)
        CancelButton.appearance().buttonColor = UIColor.darkGray
        
        let dialogAppearance = PopupDialogDefaultView.appearance()
        dialogAppearance.backgroundColor      = UIColor.darkGray
        dialogAppearance.titleFont            = UIFont.boldSystemFont(ofSize: 24)
        dialogAppearance.titleColor           = UIColor.white
        dialogAppearance.messageFont          = UIFont.systemFont(ofSize: 20)
        dialogAppearance.messageColor         = UIColor.white
        
        popup.addButtons([cancelBtn])
        
        self.present(popup, animated: true, completion: nil)
    }
    
    func showImgAnimation() {
        UIView.animate(withDuration: 2.0, delay: 2.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.bgImageView.alpha = 1
        }, completion: { _ in
        })
    }
    
    func showTextAnimation() {
        UIView.animate(withDuration: 2.0, delay: 4.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.mainTextView.alpha = 1
        }, completion: { _ in
        })
    }
    
    func showBtnAnimation() {
        UIView.animate(withDuration: 1.5, delay: 6.0, options: [UIViewAnimationOptions.curveEaseIn], animations: {
            self.mainBtn.backgroundColor = UIColor.init(red: 50.0/255.0, green: 0.0, blue: 0.0, alpha: 1.0)
            self.mainBtn.tintColor = UIColor.white
        }, completion: { _ in
            self.mainBtn.setTitle("寶藏", for: .normal)
        })
    }
}
