//
//  ForthLevelVC.swift
//  ColorTumb
//
//  Created by YUNI on 2018/4/7.
//  Copyright © 2018年 YUNI. All rights reserved.
//

import UIKit

class ForthLevelVC: BaseVC {

    @IBOutlet weak var scrollViewL1: UIScrollView!
    @IBOutlet weak var scrollViewL2: UIScrollView!
    @IBOutlet weak var scrollViewL3: UIScrollView!
    @IBOutlet weak var scrollViewR1: UIScrollView!
    @IBOutlet weak var scrollViewR2: UIScrollView!
    @IBOutlet weak var scrollViewR3: UIScrollView!
    @IBOutlet weak var btnConfirm: UIView!
    
    let scrollView_width : CGFloat = 120.0
    let scrollView_height : CGFloat = 120.0
    
    var colors : [UIColor] = [UIColor.brown, UIColor.blue, UIColor.cyan,
                              UIColor.green, UIColor.gray, UIColor.orange,
                              UIColor.red, UIColor.yellow, UIColor.magenta,
                              UIColor.gray]
    
    var scrollViewLeftColors : [Int : UIColor] = [:]
    var scrollViewRightColors : [Int : UIColor] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        playMusic(musicName: DataObject.MUSIC_NAME_BATTLE_2)
        initView()
        initScrollView(scrollView: scrollViewL1)
        initScrollView(scrollView: scrollViewL2)
        initScrollView(scrollView: scrollViewL3)
        initScrollView(scrollView: scrollViewR1)
        initScrollView(scrollView: scrollViewR2)
        initScrollView(scrollView: scrollViewR3)
        
        DispatchQueue.main.async {
            self.popupView()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        dataObj.setCurrentVC(vcName: ViewCtrlConstant.VC_FORTH_LEVEL_VC)
        initPopupLabel(title: UITextConstant.POPUP_LEVEL4_TITLE,
                       content: UITextConstant.POPUP_LEVEL4_CONTENT,
                       image: UIImage(named: DataObject.IMG_PATH_ICON_Q_POPUP)!)
    }
    
    func initView() -> Void {
        if (dataObj.getVersion() == DataObject.VERSION_RL) {
            
        } else if (dataObj.getVersion() == DataObject.VERSION_DL) {
            initHintBtn(appendView: self.view)
        }
        initQBtn(appendView: self.view)
    }
    
    @IBAction func confirmBtnPressed(_ sender: Any) {
        confirm()
    }
    
    func initScrollView(scrollView : UIScrollView) -> Void {
        scrollView.layer.cornerRadius = 10
        scrollView.backgroundColor = UIColor.clear
        scrollView.delegate = self
        scrollView.isPagingEnabled = true
        var frame : CGRect = CGRect(x:0, y:0, width: scrollView_width, height: scrollView_height)
        for index in 0..<colors.count {
            frame.origin.x = scrollView_width * CGFloat(index)
            let subView = UIView(frame: frame)
            subView.backgroundColor = colors[index]
            scrollView.addSubview(subView)
        }
        scrollView.contentSize = CGSize(width: scrollView_width * CGFloat(colors.count), height: scrollView_height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let index : Int = Int(scrollView.contentOffset.x / scrollView.frame.size.width)
        if (scrollView.tag == 1 || scrollView.tag == 2 || scrollView.tag == 3) {
            scrollViewLeftColors.updateValue(colors[index], forKey: scrollView.tag)
        } else {
            scrollViewRightColors.updateValue(colors[index], forKey: scrollView.tag)
        }
    }
    
    func confirm() -> Void {
        var isR : Bool = false
        var isG : Bool = false
        var isB : Bool = false
        for (_, color) in scrollViewLeftColors {
            if (color == UIColor.red) {
                isR = true
            } else if (color == UIColor.green) {
                isG = true
            } else if (color == UIColor.blue) {
                isB = true
            }
        }

        if (!isR || !isG || !isB) {
            alert(vc: self, title: "失敗！", message: "左方解鎖失敗。")
            return
        }

        var isC : Bool = false
        var isM : Bool = false
        var isY : Bool = false
        for (_, color) in scrollViewRightColors {
            if (color == UIColor.cyan) {
                isC = true
            } else if (color == UIColor.magenta) {
                isM = true
            } else if (color == UIColor.yellow) {
                isY = true
            }
        }

        if (!isC || !isM || !isY) {
            alert(vc: self, title: "失敗！", message: "右方解鎖失敗。")
            return
        }
        
        self.dataObj.setClearLevelFour(isClear: true)
        toNextPage(sender: self, storyboardName: ViewCtrlConstant.VC_ANSWER_VC)
    }
}
