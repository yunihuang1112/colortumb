//
//  SecondLevelVC.swift
//  ColorTumb
//
//  Created by YUNI on 2018/3/27.
//  Copyright © 2018年 YUNI. All rights reserved.
//

import UIKit

class SecondLevelVC: BaseVC {

    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var sliderView: UIView!
    @IBOutlet weak var sliderR: UISlider!
    @IBOutlet weak var sliderG: UISlider!
    @IBOutlet weak var sliderB: UISlider!
    @IBOutlet weak var btnShow: UIButton!

    @IBOutlet weak var btnY: UIButton!
    @IBOutlet weak var btnYO: UIButton!
    @IBOutlet weak var btnO: UIButton!
    @IBOutlet weak var btnOR: UIButton!
    @IBOutlet weak var btnR: UIButton!
    @IBOutlet weak var btnRP: UIButton!
    @IBOutlet weak var btnP: UIButton!
    @IBOutlet weak var btnPB: UIButton!
    @IBOutlet weak var btnB: UIButton!
    @IBOutlet weak var btnBG: UIButton!
    @IBOutlet weak var btnG: UIButton!
    @IBOutlet weak var btnGY: UIButton!
    
    var valueR : CGFloat = 0.0
    var valueG : CGFloat = 0.0
    var valueB : CGFloat = 0.0
    var count : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        playMusic(musicName: DataObject.MUSIC_NAME_BATTLE_2)
        initView()
        initQBtn(appendView: self.view)
        
        self.sliderView.isHidden = true
        self.bgImageView.image = UIImage(named: DataObject.IMG_PATH_BG_L2)
        self.btnConfirm.isHidden = true
        self.btnConfirm.addTarget(self, action: #selector(btnConfirmPressed), for: UIControlEvents.touchUpInside)
        
        setSlider(slider: self.sliderR, color: [UIColor.black.cgColor, UIColor.red.cgColor])
        setSlider(slider: self.sliderG, color: [UIColor.black.cgColor, UIColor.green.cgColor])
        setSlider(slider: self.sliderB, color: [UIColor.black.cgColor, UIColor.blue.cgColor])
        
        DispatchQueue.main.async {
            self.popupView()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        dataObj.setCurrentVC(vcName: ViewCtrlConstant.VC_SECOND_LEVEL_VC)
        initPopupLabel(title: UITextConstant.POPUP_LEVEL2_TITLE,
                       content: UITextConstant.POPUP_LEVEL2_CONTENT,
                       image: UIImage(named: DataObject.IMG_PATH_ICON_Q_POPUP)!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func setSlider(slider:UISlider, color:[Any?]) {
        let tgl = CAGradientLayer()
        let frame = CGRect(x: 0.0, y: 0.0, width: slider.bounds.width, height: 20.0 )
        tgl.frame = frame
        
        tgl.colors = color as Any as? [Any]
        
        tgl.borderWidth = 1.0
        tgl.borderColor = UIColor.gray.cgColor
        tgl.cornerRadius = 5.0
        
        tgl.endPoint = CGPoint(x: 1.0, y:  1.0)
        tgl.startPoint = CGPoint(x: 0.0, y:  1.0)
        
        UIGraphicsBeginImageContextWithOptions(tgl.frame.size, false, 0.0)
        tgl.render(in: UIGraphicsGetCurrentContext()!)
        let backgroundImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        slider.setMaximumTrackImage(backgroundImage?.resizableImage(withCapInsets:.zero),  for: .normal)
        slider.setMinimumTrackImage(backgroundImage?.resizableImage(withCapInsets:.zero),  for: .normal)
        
        let layerFrame = CGRect(x: 0, y: 0, width: 10.0, height: 30.0)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = CGPath(rect: layerFrame, transform: nil)
        shapeLayer.fillColor = UIColor.black.cgColor
        
        let thumb = CALayer.init()
        thumb.frame = layerFrame
        thumb.addSublayer(shapeLayer)
        
        UIGraphicsBeginImageContextWithOptions(thumb.frame.size, false, 0.0)
        
        thumb.render(in: UIGraphicsGetCurrentContext()!)
        let thumbImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        slider.setThumbImage(thumbImage, for: .normal)
        slider.setThumbImage(thumbImage, for: .highlighted)
        
        slider.minimumValue = 0
        slider.maximumValue = 255
        slider.addTarget(self, action: #selector(sliderAction(_:)), for: UIControlEvents.valueChanged)
    }
    
    func initView() -> Void {
        initBtn(sender: self.btnY)
        initBtn(sender: self.btnYO)
        initBtn(sender: self.btnGY)
        initBtn(sender: self.btnG)
        initBtn(sender: self.btnR)
        initBtn(sender: self.btnRP)
        initBtn(sender: self.btnOR)
        initBtn(sender: self.btnO)
        initBtn(sender: self.btnB)
        initBtn(sender: self.btnPB)
        initBtn(sender: self.btnBG)
        initBtn(sender: self.btnP)
    }
    
    func initBtn(sender: UIButton) -> Void {
        sender.alpha = 0
        sender.addTarget(self, action: #selector(btnPressed), for: UIControlEvents.touchUpInside)
    }
    
    @IBAction func showBtnPressed(_ sender: Any) {
        self.sliderView.isHidden = !self.sliderView.isHidden
    }
    
    @objc func btnPressed(sender: UIButton) {
        sender.isHidden = true
        count += 1
        if (count == 12) {
            self.btnConfirm.isHidden = false
        }
    }
    
    @objc func sliderAction(_ sender: UISlider) {
        self.bgImageView.image = UIImage(named: DataObject.IMG_PATH_BG_L2_BLANK)
        /*
         ** tag1 => R
         ** tag2 => G
         ** tag3 => B
         */
        if (sender.tag == 1) {
            valueR = CGFloat(sender.value) / CGFloat(255.0);
            UIView.animate(withDuration: 2.0, delay: 3.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                self.btnR.alpha = 1
                self.btnRP.alpha = 1
                self.btnOR.alpha = 1
                self.btnO.alpha = 1
            }, completion: nil)
        } else if (sender.tag == 2) {
            valueG = CGFloat(sender.value) / CGFloat(255.0);
            UIView.animate(withDuration: 2.0, delay: 3.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                self.btnY.alpha = 1
                self.btnYO.alpha = 1
                self.btnGY.alpha = 1
                self.btnG.alpha = 1
            }, completion: nil)
        } else if (sender.tag == 3) {
            valueB = CGFloat(sender.value) / CGFloat(255.0);
            UIView.animate(withDuration: 2.0, delay: 3.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                self.btnB.alpha = 1
                self.btnPB.alpha = 1
                self.btnBG.alpha = 1
                self.btnP.alpha = 1
            }, completion: nil)
        }
        setImgViewBgColor()
    }
    
    @objc func btnConfirmPressed(sender : UIButton) {
        appDelegate.audioPlayer = audioPlayer
        dataObj.setClearLevelTwo(isClear: true)
        toNextPage(sender: self, storyboardName: ViewCtrlConstant.VC_ANSWER_VC)
    }

    func setImgViewBgColor() -> Void {
        self.bgImageView.backgroundColor = UIColor(red: valueR, green: valueG, blue: valueB, alpha: 1.0)
    }
}

//class KeysCode: NSObject {
////    let float_full : CGFloat = 255.0
////    let float_half : CGFloat = 127.5
////    let float_zero : CGFloat = 0.0
////    let float_quar3 : CGFloat = 191.25
////    let float_quar : CGFloat = 63.75
//
//    var arr : [String : Array<CGFloat>] = [:]
//
//    override init() {
//        self.arr.updateValue([244, 229, 0],
//                             forKey: DataObject.KEY_COLOR_YELLOW)
//        self.arr.updateValue([253, 198, 11],
//                             forKey: DataObject.KEY_COLOR_YELLOW_ORANGE)
//        self.arr.updateValue([241, 145, 1],
//                             forKey: DataObject.KEY_COLOR_ORANGE)
//        self.arr.updateValue([234, 98, 31],
//                             forKey: DataObject.KEY_COLOR_ORANGE_RED)
//        self.arr.updateValue([227, 35, 34],
//                             forKey: DataObject.KEY_COLOR_RED)
//        self.arr.updateValue([196, 3, 125],
//                             forKey: DataObject.KEY_COLOR_RED_PURPLE)
//        self.arr.updateValue([109, 56, 137],
//                             forKey: DataObject.KEY_COLOR_PURPLE)
//        self.arr.updateValue([68, 78, 153],
//                             forKey: DataObject.KEY_COLOR_PURPLE_BLUE)
//        self.arr.updateValue([38, 113, 178],
//                             forKey: DataObject.KEY_COLOR_BLUE)
//        self.arr.updateValue([6, 150, 187],
//                             forKey: DataObject.KEY_COLOR_BLUE_GREEN)
//        self.arr.updateValue([0, 142, 91],
//                             forKey: DataObject.KEY_COLOR_GREEN)
//        self.arr.updateValue([140, 187, 38],
//                             forKey: DataObject.KEY_COLOR_GREEN_YELLOW)
//
////        self.arr.updateValue([float_full, float_full, 0],
////                             forKey: DataObject.KEY_COLOR_YELLOW)
////        self.arr.updateValue([float_full, float_quar3, 0],
////                             forKey: DataObject.KEY_COLOR_YELLOW_ORANGE)
////        self.arr.updateValue([float_full, float_half, 0],
////                             forKey: DataObject.KEY_COLOR_ORANGE)
////        self.arr.updateValue([float_full, float_quar, 0],
////                             forKey: DataObject.KEY_COLOR_ORANGE_RED)
////        self.arr.updateValue([float_full, 0, 0],
////                             forKey: DataObject.KEY_COLOR_RED)
////        self.arr.updateValue([0, 0, 0],
////                             forKey: DataObject.KEY_COLOR_RED_PURPLE)
////        self.arr.updateValue([0, 0, 0],
////                             forKey: DataObject.KEY_COLOR_PURPLE)
////        self.arr.updateValue([0, 0, 0],
////                             forKey: DataObject.KEY_COLOR_PURPLE_BLUE)
////        self.arr.updateValue([0, 0, float_full],
////                             forKey: DataObject.KEY_COLOR_BLUE)
////        self.arr.updateValue([0, 0, 0],
////                             forKey: DataObject.KEY_COLOR_BLUE_GREEN)
////        self.arr.updateValue([0, float_full, 0],
////                             forKey: DataObject.KEY_COLOR_GREEN)
////        self.arr.updateValue([0, 0, 0],
////                             forKey: DataObject.KEY_COLOR_GREEN_YELLOW)
//    }
//}





















