//
//  ThirdLevelVC.swift
//  ColorTumb
//
//  Created by YUNI on 2018/4/4.
//  Copyright © 2018年 YUNI. All rights reserved.
//

import UIKit

class ThirdLevelVC: BaseVC {

    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var btnY: UIButton!
    @IBOutlet weak var btnYO: UIButton!
    @IBOutlet weak var btnO: UIButton!
    @IBOutlet weak var btnOR: UIButton!
    @IBOutlet weak var btnR: UIButton!
    @IBOutlet weak var btnRP: UIButton!
    @IBOutlet weak var btnP: UIButton!
    @IBOutlet weak var btnPB: UIButton!
    @IBOutlet weak var btnB: UIButton!
    @IBOutlet weak var btnBG: UIButton!
    @IBOutlet weak var btnG: UIButton!
    @IBOutlet weak var btnGY: UIButton!
    @IBOutlet weak var rightView: RightView!
    @IBOutlet weak var btnBag: UIButton!
    @IBOutlet weak var btnConfirm: UIButton!
    
    var count : Int = 0
    var correctList : [Int : String] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        playMusic(musicName: DataObject.MUSIC_NAME_BATTLE_1)
        initView()
        initData()
        DispatchQueue.main.async {
            self.popupView()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        dataObj.setCurrentVC(vcName: ViewCtrlConstant.VC_THIRD_LEVEL_VC)
        initPopupLabel(title: UITextConstant.POPUP_LEVEL3_TITLE,
                       content: UITextConstant.POPUP_LEVEL3_CONTENT,
                       image: UIImage(named: DataObject.IMG_PATH_ICON_Q_POPUP)!)
    }
    
    fileprivate func initView() {
        if (dataObj.getVersion() == DataObject.VERSION_RL) {
            
        } else if (dataObj.getVersion() == DataObject.VERSION_DL) {
            initHintBtn(appendView: self.view)
        }
        
        initTableList(type: DataObject.TYPE_COLOR_KEY)
        initRightView(vc: self, rightView: self.rightView)
        initViewBorder(view: self.rightView)
        initBagBtn()
        initQBtn(appendView: self.view)
        
        initBtn(btn: self.btnY)
        initBtn(btn: self.btnYO)
        initBtn(btn: self.btnO)
        initBtn(btn: self.btnOR)
        initBtn(btn: self.btnR)
        initBtn(btn: self.btnRP)
        initBtn(btn: self.btnP)
        initBtn(btn: self.btnPB)
        initBtn(btn: self.btnB)
        initBtn(btn: self.btnBG)
        initBtn(btn: self.btnG)
        initBtn(btn: self.btnGY)
        
        self.btnConfirm.isHidden = true
    }
    
    fileprivate func initData() -> Void {
        correctList.updateValue(DataObject.KEY_COLOR_YELLOW, forKey: 7)
        correctList.updateValue(DataObject.KEY_COLOR_YELLOW_ORANGE, forKey: 8)
        correctList.updateValue(DataObject.KEY_COLOR_ORANGE, forKey: 9)
        correctList.updateValue(DataObject.KEY_COLOR_ORANGE_RED, forKey: 10)
        correctList.updateValue(DataObject.KEY_COLOR_RED, forKey: 11)
        correctList.updateValue(DataObject.KEY_COLOR_RED_PURPLE, forKey: 12)
        correctList.updateValue(DataObject.KEY_COLOR_PURPLE, forKey: 1)
        correctList.updateValue(DataObject.KEY_COLOR_PURPLE_BLUE, forKey: 2)
        correctList.updateValue(DataObject.KEY_COLOR_BLUE, forKey: 3)
        correctList.updateValue(DataObject.KEY_COLOR_BLUE_GREEN, forKey: 4)
        correctList.updateValue(DataObject.KEY_COLOR_GREEN, forKey: 5)
        correctList.updateValue(DataObject.KEY_COLOR_GREEN_YELLOW, forKey: 6)
    }
    
    @IBAction func confirmBtnPressed(_ sender: Any) {
        appDelegate.audioPlayer = audioPlayer
        dataObj.setClearLevelThree(isClear: true)
        toNextPage(sender: self, storyboardName: ViewCtrlConstant.VC_ANSWER_VC)
    }
    
    func initBagBtn() -> Void {
        self.btnBag.layer.cornerRadius = btnCornerRadius
        self.btnBag.addTarget(self, action: #selector(bagButtonPressed), for: UIControlEvents.touchUpInside)
    }
    
    @objc func bagButtonPressed(sender : UIButton) {
        bagBtnPressed(sender: sender, rightView: self.rightView)
    }
    
    func initBtn(btn : UIButton) -> Void {
        btn.setTitle("", for: UIControlState.normal)
        btn.addTarget(self, action: #selector(btnPressed), for: UIControlEvents.touchUpInside)
    }
    
    @objc func btnPressed(sender : UIButton) {
        if (rightListSelected != nil) {
//            if (confirmImg(senderImg: sender.currentBackgroundImage!)) {
            if (rightListSelectedImgName == correctList[sender.tag]) {
                if (rightListSelected != UIImage.init() && rightSelectedIndex >= 0) {
                    rightTableList.remove(at: rightSelectedIndex)
                    sender.isHidden = true
                    count += 1
                }
            } else {
                popErrorAlert(vc: self)
            }
            
            rightListSelected = nil
            rightSelectedIndex = -1
            reloadRightTableView(rightView: self.rightView.subviews[0] as! RightView)
            
            if (count > 8) {
                self.bgImageView.image = UIImage(named: DataObject.IMG_PATH_BG_L3_POST)
                self.btnConfirm.isHidden = false
            }
        }
    }

//    func confirmImg(senderImg : UIImage) -> Bool {
//        if (checkImg(image1: senderImg, image2: UIImage(named: DataObject.IMG_PATH_LOCK_YELLOW)!)) {
//            if (checkImg(image1: rightListSelected!, image2: UIImage(named: DataObject.IMG_PATH_KEY_PURPLE)!)) {
//                return true
//            }
//        } else if (checkImg(image1: senderImg, image2: UIImage(named: DataObject.IMG_PATH_LOCK_YELLOW_ORANGE)!)) {
//            if (checkImg(image1: rightListSelected!, image2: UIImage(named: DataObject.IMG_PATH_KEY_PURPLE_BLUE)!)) {
//                return true
//            }
//        } else if (checkImg(image1: senderImg, image2: UIImage(named: DataObject.IMG_PATH_LOCK_ORANGE)!)) {
//            if (checkImg(image1: rightListSelected!, image2: UIImage(named: DataObject.IMG_PATH_KEY_BLUE)!)) {
//                return true
//            }
//        } else if (checkImg(image1: senderImg, image2: UIImage(named: DataObject.IMG_PATH_LOCK_ORANGE_RED)!)) {
//            if (checkImg(image1: rightListSelected!, image2: UIImage(named: DataObject.IMG_PATH_KEY_BLUE_GREEN)!)) {
//                return true
//            }
//        } else if (checkImg(image1: senderImg, image2: UIImage(named: DataObject.IMG_PATH_LOCK_RED)!)) {
//            if (checkImg(image1: rightListSelected!, image2: UIImage(named: DataObject.IMG_PATH_KEY_GREEN)!)) {
//                return true
//            }
//        } else if (checkImg(image1: senderImg, image2: UIImage(named: DataObject.IMG_PATH_LOCK_RED_PURPLE)!)) {
//            if (checkImg(image1: rightListSelected!, image2: UIImage(named: DataObject.IMG_PATH_KEY_GREEN_YELLOW)!)) {
//                return true
//            }
//        } else if (checkImg(image1: senderImg, image2: UIImage(named: DataObject.IMG_PATH_LOCK_PURPLE)!)) {
//            if (checkImg(image1: rightListSelected!, image2: UIImage(named: DataObject.IMG_PATH_KEY_YELLOW)!)) {
//                return true
//            }
//        } else if (checkImg(image1: senderImg, image2: UIImage(named: DataObject.IMG_PATH_LOCK_PURPLE_BLUE)!)) {
//            if (checkImg(image1: rightListSelected!, image2: UIImage(named: DataObject.IMG_PATH_KEY_YELLOW_ORANGE)!)) {
//                return true
//            }
//        } else if (checkImg(image1: senderImg, image2: UIImage(named: DataObject.IMG_PATH_LOCK_BLUE)!)) {
//            if (checkImg(image1: rightListSelected!, image2: UIImage(named: DataObject.IMG_PATH_KEY_ORANGE)!)) {
//                return true
//            }
//        } else if (checkImg(image1: senderImg, image2: UIImage(named: DataObject.IMG_PATH_LOCK_BLUE_GREEN)!)) {
//            if (checkImg(image1: rightListSelected!, image2: UIImage(named: DataObject.IMG_PATH_KEY_ORANGE_RED)!)) {
//                return true
//            }
//        } else if (checkImg(image1: senderImg, image2: UIImage(named: DataObject.IMG_PATH_LOCK_GREEN)!)) {
//            if (checkImg(image1: rightListSelected!, image2: UIImage(named: DataObject.IMG_PATH_KEY_RED)!)) {
//                return true
//            }
//        } else if (checkImg(image1: senderImg, image2: UIImage(named: DataObject.IMG_PATH_LOCK_GREEN_YELLOW)!)) {
//            if (checkImg(image1: rightListSelected!, image2: UIImage(named: DataObject.IMG_PATH_KEY_RED_PURPLE)!)) {
//                return true
//            }
//        }
//        return false
//    }
//
//    func checkImg(image1: UIImage, image2: UIImage) -> Bool {
//        let data1: NSData = UIImagePNGRepresentation(image1)! as NSData
//        let data2: NSData = UIImagePNGRepresentation(image2)! as NSData
//        return data1.isEqual(data2)
//
//    }

}






















