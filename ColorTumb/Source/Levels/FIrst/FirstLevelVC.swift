//
//  FirstLevelVC.swift
//  ColorTumb
//
//  Created by YUNI on 2018/3/22.
//  Copyright © 2018年 YUNI. All rights reserved.
//

import UIKit
import PopupDialog

class FirstLevelVC: BaseVC {

    @IBOutlet weak var btnView: UIView!
    @IBOutlet weak var btnY: UIButton!
    @IBOutlet weak var btnYO: UIButton!
    @IBOutlet weak var btnO: UIButton!
    @IBOutlet weak var btnOR: UIButton!
    @IBOutlet weak var btnR: UIButton!
    @IBOutlet weak var btnRP: UIButton!
    @IBOutlet weak var btnP: UIButton!
    @IBOutlet weak var btnPB: UIButton!
    @IBOutlet weak var btnB: UIButton!
    @IBOutlet weak var btnBG: UIButton!
    @IBOutlet weak var btnG: UIButton!
    @IBOutlet weak var btnGY: UIButton!
    
    @IBOutlet weak var btnBag: UIButton!
    @IBOutlet weak var rightView: RightView!
    @IBOutlet weak var btnConfirm: UIButton!
    
    var defaultBtnTags : [Int] = []
    var btnTags : [Int] = []
    let correctCrystalArr : [String] = [
        DataObject.IMG_PATH_COLOR_YELLOW,
        DataObject.IMG_PATH_COLOR_YELLOW_ORANGE,
        DataObject.IMG_PATH_COLOR_ORANGE,
        DataObject.IMG_PATH_COLOR_ORANGE_RED,
        DataObject.IMG_PATH_COLOR_RED,
        DataObject.IMG_PATH_COLOR_RED_PURPLE,
        DataObject.IMG_PATH_COLOR_PURPLE,
        DataObject.IMG_PATH_COLOR_PURPLE_BLUE,
        DataObject.IMG_PATH_COLOR_BLUE,
        DataObject.IMG_PATH_COLOR_BLUE_GREEN,
        DataObject.IMG_PATH_COLOR_GREEN,
        DataObject.IMG_PATH_COLOR_GREEN_YELLOW
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.async {
            self.popupView()
        }
        
        if (appDelegate.audioPlayer == nil) {
            playMusic(musicName: DataObject.MUSIC_NAME_BATTLE_1)
        } else {
            appDelegate.audioPlayer?.play()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        initView()
        initData()
        dataObj.setCurrentVC(vcName: ViewCtrlConstant.VC_FIRST_LEVEL_VC)
        initPopupLabel(title: UITextConstant.POPUP_LEVEL1_TITLE,
                       content: UITextConstant.POPUP_LEVEL1_CONTENT,
                       image: UIImage(named: DataObject.IMG_PATH_ICON_Q_POPUP)!)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    fileprivate func initView() {
        if (dataObj.getVersion() == DataObject.VERSION_RL) {
            
        } else if (dataObj.getVersion() == DataObject.VERSION_DL) {
            initHintBtn(appendView: self.view)
        }
        
        initTableList(type: DataObject.TYPE_CRYSTAL)
        initRightView(vc: self, rightView: self.rightView)
        initViewBorder(view: self.rightView)
        initBagBtn()
        initQBtn(appendView: self.view)
        
        initBtn(btn: self.btnY)
        initBtn(btn: self.btnYO)
        initBtn(btn: self.btnO)
        initBtn(btn: self.btnOR)
        initBtn(btn: self.btnR)
        initBtn(btn: self.btnRP)
        initBtn(btn: self.btnP)
        initBtn(btn: self.btnPB)
        initBtn(btn: self.btnB)
        initBtn(btn: self.btnBG)
        initBtn(btn: self.btnG)
        initBtn(btn: self.btnGY)
        
        self.btnConfirm.isHidden = true
    }
    
    fileprivate func initData() -> Void {
        btnTags = []
        initHistoryDic()
        
        // default
        initDefaultbtn(btn: self.btnY, imgName: DataObject.IMG_PATH_COLOR_YELLOW)
        initDefaultbtn(btn: self.btnYO, imgName: DataObject.IMG_PATH_COLOR_YELLOW_ORANGE)
        initDefaultbtn(btn: self.btnRP, imgName: DataObject.IMG_PATH_COLOR_RED_PURPLE)
        initDefaultbtn(btn: self.btnB, imgName: DataObject.IMG_PATH_COLOR_BLUE)
        initDefaultbtn(btn: self.btnG, imgName: DataObject.IMG_PATH_COLOR_GREEN)
    }
    
    fileprivate func initHistoryDic() -> Void {
        for btn in self.btnView.subviews {
            if (btn.isKind(of: UIButton.self)) {
                let button : UIButton = btn as! UIButton
                let btnTag : Int = button.tag
                if (btnTag >= 0 && !defaultBtnTags.contains(btnTag)) {
                    let imgName : String = dataObj.getUserCrystals(key: btnTag)
                    if (!imgName.isEmpty) {
                        let image : UIImage = list[imgName]!
                        button.setBackgroundImage(image, for: UIControlState.normal)
                        btnTags.append(btnTag)
                        if let index = rightTableList.index(of: image) {
                            rightTableList.remove(at: index)
                        }
                    }
                }
            }
        }
    }
    
    fileprivate func initBtn(btn : UIButton) -> Void {
        btn.setBackgroundImage(nil, for:UIControlState.normal)
        btn.setTitle("", for: UIControlState.normal)
        btn.addTarget(self, action: #selector(colorBtnPressed), for: UIControlEvents.touchUpInside)
    }
    
    fileprivate func initDefaultbtn(btn: UIButton, imgName: String) -> Void {
        btn.setBackgroundImage(UIImage(named: imgName), for:UIControlState.normal)
        btn.isUserInteractionEnabled = false
        defaultBtnTags.append(btn.tag)
        btnTags.append(btn.tag)
    }
    
    fileprivate func initBagBtn() -> Void {
        self.btnBag.layer.cornerRadius = btnCornerRadius
        self.btnBag.addTarget(self, action: #selector(bagButtonPressed), for: UIControlEvents.touchUpInside)
    }
    
    @objc fileprivate func bagButtonPressed(sender : UIButton) {
        bagBtnPressed(sender: sender, rightView: self.rightView)
    }
    
    @IBAction func confirmBtnPressed(_ sender: Any) {
        appDelegate.audioPlayer = audioPlayer
        
        for index in 0...11 {
            dataObj.setUserCrystals(key: index, value: "")
        }
        dataObj.setClearLevelOne(isClear: true)
        
        toNextPage(sender: self, storyboardName: ViewCtrlConstant.VC_ANSWER_VC)
    }
    
    @IBAction func gotoArBtnPressed(_ sender: Any) {        
        appDelegate.audioPlayer = audioPlayer
        dataObj.setClearLevelOne(isClear: true)
        toNextPage(sender: self, storyboardName: ViewCtrlConstant.VC_COLOR_DETECTION_VC)
    }
    
    @objc fileprivate func colorBtnPressed(sender : UIButton) {
        if ((sender.backgroundImage(for: UIControlState.normal)) == nil) {
            if (rightListSelected != UIImage.init() && rightSelectedIndex >= 0) {
                sender.setBackgroundImage(rightListSelected, for:UIControlState.normal)
                if (rightTableList.count > rightSelectedIndex) {
                    rightTableList.remove(at: rightSelectedIndex)
                }
                btnTags.append(sender.tag)
                dataObj.setUserCrystals(key: sender.tag, value: rightListSelectedImgName)
            }
        } else {
            rightTableList.append(sender.backgroundImage(for: UIControlState.normal)!)
            sender.setBackgroundImage(nil, for:UIControlState.normal)
            if let index = btnTags.index(of: sender.tag) {
                btnTags.remove(at: index)
                dataObj.setUserCrystals(key: sender.tag, value: "")
            }
        }
        
        rightListSelected = UIImage.init()
        rightSelectedIndex = -1
        reloadRightTableView(rightView: self.rightView.subviews[0] as! RightView)
        
        confirm()
    }
    
    fileprivate func confirm() {
        if (btnTags.count > 11) {
            for btn in self.btnView.subviews {
                if (btn.isKind(of: UIButton.self)) {
                    let button : UIButton = btn as! UIButton
                    let btnTag : Int = button.tag
                    if (btnTags.contains(btnTag)) {
                        let isCorrect : Bool = confirmBtn(btn: button)
                        self.btnConfirm.isHidden = !isCorrect
                        if (!isCorrect) {
                            popErrorAlert(vc: self)
                            break
                        }
                    }
                }
            }
        }
    }

    fileprivate func confirmBtn(btn : UIButton) -> Bool {
        let image : UIImage = UIImage(named: correctCrystalArr[btn.tag])!
        if ((btn.backgroundImage(for: UIControlState.normal)) == image) {
            return true
        }
        return false
    }
}



//        if (!confirmBtn(btn: self.btnY, imgName: DataObject.IMG_PATH_COLOR_YELLOW)) { isCorrect = false }
//        if (!confirmBtn(btn: self.btnYO, imgName: DataObject.IMG_PATH_COLOR_YELLOW_ORANGE)) { isCorrect = false }
//        if (!confirmBtn(btn: self.btnO, imgName: DataObject.IMG_PATH_COLOR_ORANGE)) { isCorrect = false }
//        if (!confirmBtn(btn: self.btnOR, imgName: DataObject.IMG_PATH_COLOR_ORANGE_RED)) { isCorrect = false }
//        if (!confirmBtn(btn: self.btnR, imgName: DataObject.IMG_PATH_COLOR_RED)) { isCorrect = false }
//        if (!confirmBtn(btn: self.btnRP, imgName: DataObject.IMG_PATH_COLOR_RED_PURPLE)) { isCorrect = false }
//        if (!confirmBtn(btn: self.btnP, imgName: DataObject.IMG_PATH_COLOR_PURPLE)) { isCorrect = false }
//        if (!confirmBtn(btn: self.btnPB, imgName: DataObject.IMG_PATH_COLOR_PURPLE_BLUE)) { isCorrect = false }
//        if (!confirmBtn(btn: self.btnB, imgName: DataObject.IMG_PATH_COLOR_BLUE)) { isCorrect = false }
//        if (!confirmBtn(btn: self.btnBG, imgName: DataObject.IMG_PATH_COLOR_BLUE_GREEN)) { isCorrect = false }
//        if (!confirmBtn(btn: self.btnG, imgName: DataObject.IMG_PATH_COLOR_GREEN)) { isCorrect = false }
//        if (!confirmBtn(btn: self.btnGY, imgName: DataObject.IMG_PATH_COLOR_GREEN_YELLOW)) { isCorrect = false }
