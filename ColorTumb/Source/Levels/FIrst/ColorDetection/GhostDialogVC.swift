//
//  GhostDialogVC.swift
//  ColorTumb
//
//  Created by YUNI on 2018/4/16.
//  Copyright © 2018年 YUNI. All rights reserved.
//

import UIKit
import PopupDialog

class GhostDialogVC: BaseVC {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var ghostImgView: UIImageView!
    @IBOutlet weak var tipImgView: UIImageView!
    @IBOutlet weak var labelOne: UILabel!
    @IBOutlet weak var labelTwo: UILabel!
    @IBOutlet weak var labelHint: UILabel!

    public var type : String?
    public var ghostImg : UIImage?
    
    var inputText : String = ""
    var popupImg : UIImage = UIImage.init()
    var crystalDic : [String : Bool] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        playMusic(musicName: DataObject.MUSIC_NAME_BATTLE_2)
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    fileprivate func initView() -> Void {
        self.labelHint.alpha = 0
        if (ghostImg == nil || type == nil) {
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        self.backView.layer.cornerRadius = 10.0
        if (dataObj.getGhostType(type: type!)) {
            self.ghostImgView.image = UIImage(named: DataObject.IMG_GHOST_ANGRY)
            self.labelOne.text = UITextConstant.GHOST_DIALOG_NONE_MSG_1
            self.labelTwo.text = UITextConstant.GHOST_DIALOG_NONE_MSG_2
        } else {
            self.ghostImgView.image = ghostImg
            initLabel()
            initInputText()
            showHintAnimation()
            self.becomeFirstResponder()
        }
    }
    
    fileprivate func initLabel() -> Void {
        if (type == DataObject.IMG_GHOST_R || type == DataObject.IMG_GHOST_Y || type == DataObject.IMG_GHOST_M) {
            self.labelOne.text = UITextConstant.GHOST_DIALOG_WARM
            self.labelTwo.text = UITextConstant.GHOST_DIALOG_FEEL_WARM
        } else if (type == DataObject.IMG_GHOST_B || type == DataObject.IMG_GHOST_C) {
            self.labelOne.text = UITextConstant.GHOST_DIALOG_COLD
            self.labelTwo.text = UITextConstant.GHOST_DIALOG_FEEL_COLD
        } else if (type == DataObject.IMG_GHOST_G) {
            self.labelOne.text = UITextConstant.GHOST_DIALOG_NONE
            self.labelTwo.text = UITextConstant.GHOST_DIALOG_FEEL_NONE
        }
    }
    
    fileprivate func initInputText() -> Void {
        if (type == DataObject.IMG_GHOST_R) {
            inputText = "紅色" + UITextConstant.INPUT_TEXT_WARM
        } else if (type == DataObject.IMG_GHOST_Y) {
            inputText = "黃色" + UITextConstant.INPUT_TEXT_WARM
        } else if (type == DataObject.IMG_GHOST_M) {
            inputText = "洋紅色" + UITextConstant.INPUT_TEXT_WARM
        } else if (type == DataObject.IMG_GHOST_B) {
            inputText = "藍色" + UITextConstant.INPUT_TEXT_COLD
        } else if (type == DataObject.IMG_GHOST_C) {
            inputText = "青色" + UITextConstant.INPUT_TEXT_COLD
        }  else if (type == DataObject.IMG_GHOST_G) {
            inputText = "綠色" + UITextConstant.INPUT_TEXT_NONE
        }
    }
    
    fileprivate func showHintAnimation() -> Void {
        UIView.animate(withDuration: 1.5, delay: 2.0, options: [UIViewAnimationOptions.autoreverse, UIViewAnimationOptions.repeat], animations: {
            self.labelHint.alpha = 1
        }, completion: nil)
    }
    
    override var canBecomeFirstResponder: Bool {
        get {
            return true
        }
    }
    
    func popupCustomView() -> Void {
        popupTitleStr = UITextConstant.POPUP_GHOST_TITLE
        popupContentStr = UITextConstant.POPUP_GHOST_CONTENT
        popupImage = popupImg
        
        let title = popupTitleStr
        let message = popupContentStr
        let image = popupImage
        
        if (title == "" || message == "" || image == UIImage.init()) {
            print("popup error : no str or image")
            return
        }
        
        let popup = PopupDialog(title: title, message: message, image: image)
        let cancelBtn = CancelButton(title: UITextConstant.POPUP_HINT_BTN_OK) {
            self.navigationController?.popViewController(animated: true)
        }
        
        PopupDialogContainerView.appearance().backgroundColor = UIColor.clear
        CancelButton.appearance().titleColor  = UIColor.white
        CancelButton.appearance().titleFont   = UIFont.boldSystemFont(ofSize: 16)
        CancelButton.appearance().buttonColor = UIColor(red:0.5, green:0.5, blue:0.5, alpha:0.5)
        
        let dialogAppearance = PopupDialogDefaultView.appearance()
        dialogAppearance.backgroundColor      = UIColor(red:0.5, green:0.5, blue:0.5, alpha:0.5)
        dialogAppearance.titleFont            = UIFont.boldSystemFont(ofSize: 24)
        dialogAppearance.titleColor           = UIColor.white
        dialogAppearance.messageFont          = UIFont.systemFont(ofSize: 20)
        dialogAppearance.messageColor         = UIColor.white
        
        popup.addButtons([cancelBtn])
        
        self.present(popup, animated: true, completion: nil)
    }
    
    func setKeyValues(key : String, value : Bool) -> Void {
        crystalDic.updateValue(value, forKey: key)
    }
    
    func changeCrystalDatas(trueStrings: [String]) -> Void {
        for (key , value) in crystalDic {
            if (trueStrings.contains(key)) {
                setKeyValues(key: key, value: true)
            } else {
                setKeyValues(key: key, value: value)
            }
        }
        dataObj.setCrystalDic(dic: crystalDic)
    }
    
    fileprivate func success() -> Void {
        crystalDic = dataObj.getCrystalDic()
        if (type == DataObject.IMG_GHOST_R) {
            changeCrystalDatas(trueStrings: [DataObject.COLOR_ORANGE, DataObject.COLOR_ORANGE_RED, DataObject.COLOR_RED])
            popupImg = UIImage(named: DataObject.IMG_GHOST_GAIN_R)!
            dataObj.setGhostType(type: DataObject.IMG_GHOST_R, value: true)
        } else if (type == DataObject.IMG_GHOST_Y) {
            changeCrystalDatas(trueStrings: [DataObject.COLOR_YELLOW, DataObject.COLOR_YELLOW_ORANGE, DataObject.COLOR_GREEN_YELLOW])
            popupImg = UIImage(named: DataObject.IMG_GHOST_GAIN_Y)!
            dataObj.setGhostType(type: DataObject.IMG_GHOST_Y, value: true)
        } else if (type == DataObject.IMG_GHOST_M) {
            changeCrystalDatas(trueStrings: [DataObject.COLOR_RED_PURPLE])
            popupImg = UIImage(named: DataObject.IMG_PATH_COLOR_RED_PURPLE)!
            dataObj.setGhostType(type: DataObject.IMG_GHOST_M, value:true)
        } else if (type == DataObject.IMG_GHOST_B) {
            changeCrystalDatas(trueStrings: [DataObject.COLOR_PURPLE_BLUE, DataObject.COLOR_BLUE])
            popupImg = UIImage(named: DataObject.IMG_GHOST_GAIN_B)!
            dataObj.setGhostType(type: DataObject.IMG_GHOST_B, value:true)
        } else if (type == DataObject.IMG_GHOST_C) {
            changeCrystalDatas(trueStrings: [DataObject.COLOR_BLUE_GREEN])
            popupImg = UIImage(named: DataObject.IMG_PATH_COLOR_BLUE_GREEN)!
            dataObj.setGhostType(type: DataObject.IMG_GHOST_C, value:true)
        }  else if (type == DataObject.IMG_GHOST_G) {
            changeCrystalDatas(trueStrings: [DataObject.COLOR_GREEN, DataObject.COLOR_PURPLE])
            popupImg = UIImage(named: DataObject.IMG_GHOST_GAIN_G)!
            dataObj.setGhostType(type: DataObject.IMG_GHOST_G, value:true)
        }
        popupCustomView()
    }
    
    fileprivate func checkInput(text: String) {
        if (text != inputText) {
            self.ghostImgView.image = UIImage(named: DataObject.IMG_GHOST_ANGRY)
        } else {
            success()
        }
    }

    fileprivate func popAlert() {
        let alert = UIAlertController(title: "回答鬼魂",
                                      message: "請輸入「" + inputText + "」",
                                      preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "請輸入答案"
        }
        alert.addAction(
            UIAlertAction(title: UITextConstant.ALERT_BTN_OK,
                          style: .default,
                          handler: { [weak alert] (_) in
                            let textField = alert?.textFields![0]
                            self.checkInput(text: (textField?.text)!)
            }))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        if (motion == .motionShake) {
            popAlert()
        }
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
