//
//  ColorDetectionVC.swift
//  ColorTumb
//
//  Created by YUNI on 2018/4/10.
//  Copyright © 2018年 YUNI. All rights reserved.
//

import UIKit
import AVFoundation

extension UIImageView {
    
    func getColorDic(at point: CGPoint) -> [String : UIColor] {
        let pixel = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: 4)
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: pixel, width: 1, height: 1, bitsPerComponent: 8, bytesPerRow: 4, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!
        
        context.translateBy(x: -point.x, y: -point.y)
        self.layer.render(in: context)
        let r : CGFloat = CGFloat(pixel[0]) / 255.0
        let g : CGFloat = CGFloat(pixel[1]) / 255.0
        let b : CGFloat = CGFloat(pixel[2]) / 255.0
        let a : CGFloat = CGFloat(pixel[3]) / 255.0
        let color = UIColor(red:   r,
                            green: g,
                            blue:  b,
                            alpha: a)
        var colorStr : String = ""
        colorStr = "R : " + String(format: "%.2f", r)  + "   G : " + String(format: "%.2f", g) + "   B : " + String(format: "%.2f", b)
        
        let hueColor : (h: CGFloat, s: CGFloat, b: CGFloat) = rgbToHue(r: r, g: g, b: b)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.hsbColor = hueColor
        
        pixel.deallocate(capacity: 4)
        
        var dic : [String : UIColor] = [:]
        dic.updateValue(color, forKey: colorStr)
        
        return dic
    }
    
    func rgbToHue(r: CGFloat, g: CGFloat, b: CGFloat) -> (h: CGFloat, s: CGFloat, b: CGFloat) {
        let minV:CGFloat = CGFloat(min(r, g, b))
        let maxV:CGFloat = CGFloat(max(r, g, b))
        let delta:CGFloat = maxV - minV
        var hue:CGFloat = 0
        if delta != 0 {
            if r == maxV {
                hue = (g - b) / delta
            }
            else if g == maxV {
                hue = 2 + (b - r) / delta
            }
            else {
                hue = 4 + (r - g) / delta
            }
            hue *= 60
            if hue < 0 {
                hue += 360
            }
        }
        let saturation = maxV == 0 ? 0 : (delta / maxV)
        let brightness = maxV
        return (h:hue/360, s:saturation, b:brightness)
    }
}

class ColorDetectionVC: UIViewController {

    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var capturedImage: UIImageView!
    @IBOutlet weak var colorBoxView: UIView!
    @IBOutlet weak var colorLabel: UILabel!
    @IBOutlet weak var ghostBtn: UIButton!
    
    var session: AVCaptureSession?
    var stillImageOutput: AVCaptureStillImageOutput?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var type : String?
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let h_r_y : CGFloat = CGFloat(1.0) / CGFloat(12.0)
    let h_y_g : CGFloat = CGFloat(3.0) / CGFloat(12.0)
    let h_g_c : CGFloat = CGFloat(5.0) / CGFloat(12.0)
    let h_c_b : CGFloat = CGFloat(7.0) / CGFloat(12.0)
    let h_b_m : CGFloat = CGFloat(9.0) / CGFloat(12.0)
    let h_m_r : CGFloat = CGFloat(11.0) / CGFloat(12.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (appDelegate.audioPlayer != nil) {
            appDelegate.audioPlayer?.play()
        }
        
        self.colorBoxView.backgroundColor = UIColor.clear
        self.colorLabel.text = ""
        self.ghostBtn.setBackgroundImage(UIImage.init(), for: UIControlState.normal)
        initCapture()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        videoPreviewLayer!.frame = previewView.bounds
    }
    
    fileprivate func initCapture() {
        session = AVCaptureSession()
        session!.sessionPreset = AVCaptureSession.Preset.photo
        let backCamera = AVCaptureDevice.default(for: AVMediaType.video)
        var error: NSError?
        var input: AVCaptureDeviceInput!
        do {
            input = try AVCaptureDeviceInput(device: backCamera!)
        } catch let error1 as NSError {
            error = error1
            input = nil
            print("capture error : " + error!.localizedDescription)
        }
        if error == nil && session!.canAddInput(input) {
            session!.addInput(input)
            stillImageOutput = AVCaptureStillImageOutput()
            stillImageOutput?.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
            if session!.canAddOutput(stillImageOutput!) {
                session!.addOutput(stillImageOutput!)
                videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session!)
                videoPreviewLayer!.videoGravity = AVLayerVideoGravity.resizeAspect
                videoPreviewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.landscapeRight
                previewView.layer.addSublayer(videoPreviewLayer!)
                session!.startRunning()
            }
        }
    }
    
    fileprivate func capture(point : Any) {
        let videoConnection = stillImageOutput!.connection(with: AVMediaType.video)
        if(videoConnection != nil) {
            stillImageOutput?.captureStillImageAsynchronously(from: videoConnection!, completionHandler: { (sampleBuffer, error) -> Void in
                if (sampleBuffer != nil) {
                    let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer!)
                    let dataProvider = CGDataProvider(data: imageData! as CFData)
                    let cgImageRef = CGImage.init(jpegDataProviderSource: dataProvider!, decode: nil, shouldInterpolate: true, intent: CGColorRenderingIntent.relativeColorimetric)
                    
                    let image = UIImage(cgImage: cgImageRef!, scale: 1.0, orientation: UIImageOrientation.up)
                    self.capturedImage.image = image
                    let colorDic : [String : UIColor] = self.capturedImage.getColorDic(at: point as! CGPoint)
                    self.colorBoxView.backgroundColor = Array(colorDic.values)[0]
                    self.colorLabel.text = Array(colorDic.keys)[0]
                    self.initGhostBtn()
                }
            })
        }
    }
    
    fileprivate func initGhostBtn() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let hsbColor : (h: CGFloat, s: CGFloat, b: CGFloat) = appDelegate.hsbColor!
        let hue : CGFloat = hsbColor.h
        let saturation : CGFloat = hsbColor.s
        let brightness : CGFloat = hsbColor.b

        if ((saturation >= 0.2 && saturation <= 0.88) && (brightness >= 0.2 && brightness <= 0.8)) {
            if (hue <= h_r_y || hue > h_m_r) {
                type = DataObject.IMG_GHOST_R
            } else if (hue <= h_y_g && hue > h_r_y) {
                type = DataObject.IMG_GHOST_Y
            } else if (hue <= h_g_c && hue > h_y_g) {
                type = DataObject.IMG_GHOST_G
            } else if (hue <= h_c_b && hue > h_g_c) {
                type = DataObject.IMG_GHOST_C
            } else if (hue <= h_b_m && hue > h_c_b) {
                type = DataObject.IMG_GHOST_B
            } else if (hue <= h_m_r && hue > h_b_m) {
                type = DataObject.IMG_GHOST_M
            }
            self.ghostBtn.setBackgroundImage(UIImage(named: type!), for: UIControlState.normal)
        } else {
            self.ghostBtn.setBackgroundImage(UIImage.init(), for: UIControlState.normal)
        }

    }
    
    fileprivate func takeSnapshot() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(previewView.bounds.size, false, UIScreen.main.scale)
        self.view.drawHierarchy(in: previewView.bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        
//        capturedImage.image = takeSnapshot()
        if let point = touch?.location(in: capturedImage) {
            if (point.x < 120 || point.x > 904 || point.y < 180 || point.y > 608) {
                //nothing
            } else {
                capture(point: point)
            }
        }
    }

    @IBAction func ghostBtnPressed(_ sender: Any) {
        appDelegate.audioPlayer?.pause()
        let vc = storyboard?.instantiateViewController(withIdentifier: ViewCtrlConstant.VC_GHOST_DIALOG_VC)
        (vc as! GhostDialogVC).ghostImg = self.ghostBtn.backgroundImage(for: UIControlState.normal)
        (vc as! GhostDialogVC).type = type
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}






